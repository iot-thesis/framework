# CScript

CScript is a domain-specific language for distributed programming which extends JavaScript [^1] with language constructs for replication and consistency. CScript embeds native support for strong consistency (CP) and availability (AP).
[^1]: CScript runs on top of the Node.js runtime.

A CScript application is a collection of services, where a service is a container of distributed objects. To keep distributed objects consistent various consistency models are provided, including strong consistency and strong eventual consistency (SEC).

## Installation

CScript is a Node.js module available through the [npm registry](https://www.npmjs.com).
Install CScript using npm:

```sh
$ npm install cscript
```

## How To Run

Since CScript is a DSL built on top of Javascript, you first need to compile your application to Javascript. CScript files must use the `.cs` extension and can be compiled using the following command:

```sh
# Compile project
$ "$(npm bin)"/csc "$PWD" <main_file> <out_directory>

# Run project
node ./compiled/<main_file>
```

The above `csc` command compiles the main file as well as all CScript files in the same directory and any of its subdirectories. After the compilation, run the project as a regular Node.js project.

## Links

* CScript [tutorial](https://gitlab.com/iot-thesis/framework/wikis/home).
* [SECRO implementation](https://gitlab.com/iot-thesis/framework/tree/master/src/Application/CRDTs/SECRO).
* [Benchmarks](https://gitlab.com/iot-thesis/benchmarks).
* [Example applications](https://gitlab.com/iot-thesis/use-cases) built on top of CScript:
* [Shared Grocery List](https://gitlab.com/iot-thesis/use-cases/grocery-list)
* Text Editors
  * [List Implementation](https://gitlab.com/iot-thesis/use-cases/collaborative-text-editors/CTE-SECRO-version)
  * [Tree Implementation](https://gitlab.com/iot-thesis/use-cases/collaborative-text-editors/Tree-CTE-SECRO-version)