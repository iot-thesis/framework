'lang sweet.js';

import { unwrap, fromStringLiteral, fromIdentifier, isKeyword, isIdentifier, fromBraces } from '@sweet-js/helpers' for syntax;
import * as stateContainer from './stateContainer.js' for syntax;

/*
 * `<=` operator for far references.
 * This is a binary operator that is left associative.
 * This operator must have the same precedence than function application/member access `.` (which has precedence 19)
 * such that `farRef<=someMethod(foo).then(result => ...)`
 * is equivalent to `(farRef<=someMethod(foo)).then(result => ...)`
 *
 * Transformation:
 * `farRef<=someMethod(foo)` is translated to `farRef.someMethod(foo)`
 */

/*
export operator <= left 19 = (left, right) => {
  console.log("left is: ");
  console.log(left);
  console.log("right is: ");
  console.log(right);
  return #`${left}.${right}`;
}; */

/*
export operator <= right 19 = (left, right) => {
  console.log('right is: ');
    console.log(right);
  let r = right.name.token.value; //unwrap(right.value).value,
  var dummy = #`dummy`.get(0);
  return #`${left} [${fromStringLiteral(dummy, r)}]`;
};
*/

/* PROBEERSEL...
export operator <= right 19 = (left, right) => {
  console.log('right is: ');
    console.log(right);

  const dummy = #`dummy`.get(0);

  if (isIdentifier(right)) {
      const r = right.name.token.value;
      return #`${left} [${fromStringLiteral(dummy, r)}]`;
  }
  else {
      // it must be a call expression
      const rName = right.callee.name.token.value,
            rArgs = right.arguments;
      console.log("left." + rName + " - args: ");
      console.log(rArgs);
      return #`${left} [${fromStringLiteral(dummy, rName)}](${...rArgs})`;
  }

  //let r = right.name.token.value; //unwrap(right.value).value,
  //var dummy = #`dummy`.get(0);
  //return #`${left} [${fromStringLiteral(dummy, r)}]`;
}; */

// `deftype typetag` macro
export syntax deftype = function(ctx) {
    let typetag = unwrap(ctx.next().value).value;

    if (stateContainer.hasTypetag(typetag)) {
        throw new Error('Parse error: duplicate definition of type ' + typetag + '.');
    }
    else {
        stateContainer.registerTypetag(typetag);
    }
    
    return #``;
}

// `publish <service> as <tag>` macro
export syntax publish = function(ctx) {
    let dummy   = #`dummy`.get(0),
        service = ctx.expand('expr').value,
        az      = unwrap(ctx.next().value).value; // eat `as`
    
    if (az !== 'as') {
        throw Error('Parse error: expected \'publish <service instance> as <typetag>\' but got something else.');
    }
    
    let typeTag       = ctx.next().value,
        typeTagString = unwrap(typeTag).value;
    
    if (!stateContainer.hasTypetag(typeTagString)) {
        throw new Error('Typetag \'' + typeTagString + '\' was not defined.');
    }
    
    return #`${service}.publish(${fromStringLiteral(dummy, typeTagString)});`;
}

// `subscribe <tag> with <callback>` macro
export syntax subscribe = function(ctx) {
    let dummy         = #`dummy`.get(0),
        typeTag       = ctx.next().value,
        typeTagString = unwrap(typeTag).value,
        withh         = unwrap(ctx.next().value).value; // eat `with`
    
    if (withh !== 'with') {
        throw Error('Parse error: expected \'subscribe <typetag> with <callback>\' but got something else.');
    }
    
    if (!stateContainer.hasTypetag(typeTagString)) {
        throw new Error('Typetag \'' + typeTagString + '\' was not defined.');
    }
    
    let callback = ctx.expand('expr').value;
    
    return #`Service.subscribe(${fromStringLiteral(dummy, typeTagString)}, ${callback})`;
}

/*
 * Pre- and post-conditions occur in classes.
 * Since SweetJS only allows macros to occur in statements and expressions,
 * we need to define our own `class` macro.
 *
 * This macro scans the body of the class, leaving everything unchanged except pre and post conditions.
 * This macro can deal with regular classes but also classes that extend a super class.
 *
 * Transformation:
 * `pre someMethod(...) { ... }` is transformed to `static preSomeMethod(...) { ... }` to conform to the naming convention.
 */
export syntax class = function(ctx) {
    let dummy = #`dummy`.get(0);
    
    // helper function
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    // Handles pre and post conditions
    function condition(type, ctx) {
        let method     = ctx.next().value,
            methodName = type + capitalizeFirstLetter(unwrap(method).value),
            params     = ctx.next().value,
            body       = ctx.next().value;
        
        return #`static ${fromIdentifier(dummy, methodName)} ${params} ${body}`;
    }
    
    let className = ctx.next().value,
        clazz     = #`class ${className}`,
        extending = ctx.next().value,
        body      = #``;
    
    var bodyCtx;
    if (isKeyword(extending)) {
        // Class extends another class
        var superClass = ctx.next().value;
        clazz = clazz.concat(#` extends ${superClass}`);
        bodyCtx = ctx.contextify(ctx.next().value);
    }
    else {
        bodyCtx = ctx.contextify(extending);
    }
    
    for (let item of bodyCtx) {
        if (isIdentifier(item) && (unwrap(item).value === 'pre' || unwrap(item).value === 'post')) {
            let type = unwrap(item).value,
                transformedMethod = condition(type, bodyCtx);
            body = body.concat(transformedMethod);
        }
        else {
            body = body.concat(#`${item}`);
        }
    }
    
    return #`${clazz} { ${body} }`;
}

/*
 
The `service` macro transform this service syntax:

service MyService {
    weak someField = 10;
    strong otherField = 50;
    
    constructor() {
        // ...
    }
    
    someMethod() {
        // ...
    }
}
 
into JS (ES6) class syntax:
 
class MyService extends Service {
    constructor() {
        super();
        
        this.someField  = super.makeEC('someField', 10);
        this.otherField = super.makeSC('otherField', 50); 
        
        // ...
    }
    
    someMethod() {
        // ...
    }
}
 
 */

export syntax service = function (ctx) {
    let dummy = #`dummy`.get(0);
    let serviceIdentifier = fromIdentifier(dummy, 'Service');
    
    /*
     * Helper function that transforms:
     *   type field = val;
     * into
     *   this.defineCharacteristic('field', type', val);
     *
     * where type = weak | strong
     * and   type' = Service.EVENTUALLY_CONSISTENT | Service.STRONGLY_CONSISTENT
     */

    function charac(ctx) { //, type) {
        let property = ctx.next().value;
        let propertyName = unwrap(property).value;
        ctx.next(); // eat `=`
        let propertyVal = ctx.expand('expr').value;

        return { property: property, propertyName: fromStringLiteral(dummy, propertyName), val: propertyVal};
    };
    
    let name = ctx.next().value, bodyCtx = ctx.contextify(ctx.next().value);
    let characteristics = #``, 
        ctorParams = #`()`, 
        ctorBody = #``, 
        methods = #``;
    
    /*
     * Inspect the service's body and store the constructor, methods and characteristics.
     * We store them such that we can later on tranform the service to a class.
     */

    for (let item of bodyCtx) {
        if (isIdentifier(item) && unwrap(item).value === 'rep') {
            var {property, propertyName, val} = charac(bodyCtx);
            var def = #`this.${property} = super.makeReplica(${propertyName}, ${val});`;
            characteristics = characteristics.concat(def);
        }
        /*
        else if (isIdentifier(item) && unwrap(item).value === 'far') {
            var {property, propertyName, val} = charac(bodyCtx);
            var def = #`this.${property} = super.makeSC(${propertyName}, ${val});`;
            characteristics = characteristics.concat(def);
        } */
        else if (isIdentifier(item) && unwrap(item).value === 'constructor') {
            ctorParams = bodyCtx.next().value;
            let ctorBodyCtx = ctx.contextify(bodyCtx.next().value);
            
            for (let item of ctorBodyCtx) {
                ctorBody = ctorBody.concat(#`${item}`);
            }
        }
        else {
            methods = methods.concat(#`${item}`);
        }
    }
                                           
    // Transform it to regular ES6 class syntax
    let result = #`class ${name} extends Service {
                     constructor ${ctorParams} {
                         super();
                         ${characteristics}
                         ${ctorBody}
                     }
                     ${methods}
                   }`; // `
    
    return result;
}