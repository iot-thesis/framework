/*
 * Implementation of a tree-based collaborative text editor using SECROs.
 * A text document is a balanced tree of characters.
 *
 * Supported operations:
 *  - insertAfter(pos|null, charac) --> pos
 *  - delete(pos) --> void
 *  - clearDocument()  --> void
 *  - getContent() --> string
 */

import * as fw from '../../../src/framework';
const accessor = fw.Decorators.accessor;
require('google-closure-library');
declare var goog;
goog.require('goog.structs.AvlTree');

/*
 * Positions in the text document are represented as numbers.
 * In a way such that the current position is always bigger than the previous one and smaller than the next one.
 * We cannot use raw indexes because of the following example:
 *   doc = acd
 *   indexOf(a) = 0, indexOf(c) = 1, indexOf(d) = 2
 *
 *   If we now insert "b" after "a" we get:
 *   doc = abcd
 *   indexOf(a) = 0, indexOf(b) = 1, indexOf(c) = 2, indexOf(d) = 3
 *
 *   Notice how the positions of the characters
 *   that come after the inserted character have all increased by one !
 *
 * We cannot afford to update the keys of the nodes of our AVL tree.
 * Therefore, we construct our positions such that we do not have to update the other positions,
 * while still guaranteeing that: position(previous) < position(current) < position(next).
 * To this end, the position of a character is defined as:
 * pos(c) = [ pos(prev(c)) + pos(next(c)) ] / 2
 * which simply is the mean of the previous and the next positions.
 * Since the SECRO orders concurrent operations in the same order at all replicas,
 * all replicas will end up in the same state and the relative order of elements is also respected.
 */
type Pos = number;

// A `Character` is a tuple: (char, pos)
class Character {
    constructor(readonly char : string, readonly pos : Pos) {}
    // Compares the positions of 2 characters.
    // Returns 0 for equal positions,
    // a negative number if this position is smaller,
    // a positive number if this position is bigger.
    static compare(char1, char2) {
        return char1.pos - char2.pos;
    }
}

class TextEditor implements CmRDT_I<TextEditor> {
    protected _docTree : any;
    constructor(str: string = "") {
        this._docTree = new goog.structs.AvlTree(Character.compare);
        var pos = null;
        for (var i = 0; i < str.length; i++)
            pos = this.insertAfter(pos, str.charAt(i));
    }

    // Returns a boolean indicating the presence or absence of the given position in the document.
    @accessor
    protected hasPosition(pos : Pos) {
        const dummyChar = {char: '', pos: pos};
        return this._docTree.contains(dummyChar);
    }

    /*
     * Inserts a character after a certain position.
     * Returns the position of the newly inserted character,
     * which is defined as:
     *  position(c) = 1 iff document is empty
     *                position(first(doc)) / 2 iff the character is prepended to the document
     *                pos+1 iff the position after which to insert is the last position in the document
     *                [ pos + position(next(pos)) ] / 2  iff the character is inserted in between two other positions.
     */
    insertAfter(pos : Pos | null, char : string) {
        if (char.length !== 1)
            throw new TypeError(`Expected a character, but got a string with a length that is not 1.`);

        const newPos  = this.determinePosition(pos),
              newChar = new Character(char, newPos);

        if (!this._docTree.add(newChar))
            throw new Error(`Insertion failed.`);
        else
            return newPos;
    }

    static preInsertAfter(state : TextEditor, pos : Pos, char : string) {
        // The position after which to insert must exist
        return pos === null || state.hasPosition(pos);
    }
    
    static postInsertAfter(state : TextEditor, originalState : TextEditor, args: Array<any>, iPos : Pos) {
        /*
         * The character we inserted must occur after the character it was inteded to be.
         * It must not occur right after it since concurrent insertions after the same element are possible.
         */
        const [pos, char] = args;

        const originalChar = {char: "dummy", pos: pos},
              insertedChar = {char: "dummy", pos: iPos};

        // state eens printen
        console.log("state is:");
        console.log(state);

        return (pos === null && state._docTree.contains(insertedChar)) ||
               state._docTree.indexOf(originalChar) < state._docTree.indexOf(insertedChar);
    }

    // Computes a new position that is bigger than `after` and smaller than `next(after)`.
    @accessor
    private determinePosition(after : Pos | null): Pos {
        if (after === null) {
            // Prepend `char` to the document
            if (this._docTree.getCount() === 0) {
                // Document is empty
                return 1;
            }
            else {
                const first = this._docTree.getMinimum(); // character with the smallest position (i.e. the first character)
                return first.pos / 2;
            }
        }
        else {
            var next = null;

            // The AVL tree's API does not allow us to get a node and then ask for its next node.
            // Therefore we could use `indexOf` to determine the index of `after` and then ask for
            // the node at index `after+1` using `getKthValue`.
            // However, this would be in O(2*log(n)), so it's more efficient to use a traversal approach
            // starting at `after` and only going to it's next element, which is in `O(log(n) + 1)`.
            this._docTree.inOrderTraverse(char => {
                next = char.pos;
                return next !== after; // stops the traversal when we are at the successor of `after`.
            }, new Character("dummy", after));

            if (next === null) {
                // `after` does not exist in the document!
                throw new Error(`Unexisting position: ${after}.`);
            }
            else if (next !== after) {
                // we will insert in between `after` and `next`
                return (after + next) / 2;
            }
            else {
                // next === after
                // `after` is the last position in the document.
                // We will append `char` to the document.
                return after + 1;
            }
        }
    }
    
    delete(pos : Pos) {
        if (this._docTree.remove(pos) === null)
            throw new Error(`Position does not exist: ${pos}.`);
    }
    
    static postDelete(state : TextEditor, originalState : TextEditor, args: Array<Pos>) {
        // Position must be deleted!
        const [pos] = args;
        return !state.hasPosition(pos);
    }

    getSize(): number {
        return this._docTree.getCount();
    }
    
    getContent(): string {
        var str = "";
        this._docTree.inOrderTraverse(char => {
            str += char.char;
            return false;
        });
        return str;
    }

    getRawContent(): Array<Character> {
        var acc = [];
        this._docTree.inOrderTraverse(char => {
            acc.push(char);
            return false;
        });
        return acc;
    }
    
    tojson() {
        return this.getRawContent();
    }
    
    static fromjson(content) {
        var editor = new TextEditor();
        content.forEach(char => editor._docTree.add(char));
        return editor;
    }
}

fw.Factory.registerCmRDTClass(TextEditor);

export {Character, TextEditor, fw};