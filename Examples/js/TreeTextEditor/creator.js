/* 
 * Run this creator:
 * $ ../../../run.sh creator
 * Then open 'textEditor.html' in the browser.
 */

import { init, pushUpdate } from './util';

/*
 * Create a TextEditor service and publish it.
 */

import {TextEditor, Character, fw} from './treeLogic';

class TextEditorService extends fw.Service {
    constructor(n) {
        super();
        this.textEditor = super.makeReplica('textEditor', new TextEditor());
    }
}

var editorService = new TextEditorService();
init(editorService);

editorService.textEditor.onUpdate(pushUpdate);
editorService.publish('TextEditor');