/* 
 * Run this editor:
 * $ ../../../run.sh editor.js
 * Then open 'textEditor.html' in the browser.
 */

import { init, pushUpdate } from './util';

/*
 * Subscribe to text editor services.
 */

import {TextEditor, Character, fw} from './treeLogic';

var editorService = null;
fw.Service.subscribe('TextEditor', editor => {
    if (editorService === null) {
        init(editor);
        console.log(`Received text editor service. You can now start editing the document`);
        editorService = editor;
        editor.textEditor.onUpdate(pushUpdate);
    }
    else {
        console.log('[WARNING]: Already found a text editor service. Ignoring this one.');
    }
});