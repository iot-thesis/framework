/*
 * Implementation of a collaborative text editor as a SECRO (Strong Eventually Consistent Replicated Object).
 * A text document is a sequence of characters.
 *
 * Supported operations:
 *  - insertAfter(characID, charac) --> characID
 *  - delete(characID) --> void
 *  - clearDocument()  --> void
 *  - getContent() --> string
 */

import * as fw from '../../../src/framework';

// Representation of a character in a text document
var characterCounter = 1;
class Character {
    id      : string;
    char    : string; // string of length 1!
    visible : boolean;
    
    constructor(c : string, id? : string, visible? : boolean) {
        if (c.length !== 1) { throw new TypeError(`Expected a character, but got a string with a length that is not 1.`); }
        this.char    = c;
        this.visible = visible ? visible : true;
        this.id      = id ? id : `${fw.ipAddress}:${characterCounter++}`;
    }
}

class TextEditor implements CmRDT_I<TextEditor> {
    protected _content : any;
    constructor(initialContent: string = "") {
        this._content = initialContent.split('').map(char => new Character(char));
    }

    // If the index of the character after which to insert is known, then it is faster to provide it.
    insertAfter(characterID : string|null, character : Character, index? : number) {
        character = JSON.parse(JSON.stringify(character)); // Because the received arguments are immutable!

        if (!characterID) {
            // Insert at the beginning of the document
            this._content.splice(0, 0, character);
        }
        else {
            /*
            if (this.getCharacter(character.id)) {
                throw new Error(`Characters must be unique, cannot insert the same character multiple times.`);
            }
            */

            var idx = index === null ? this.getCharacterIndex(characterID) : index;
            if (idx != -1) {
                // Found
                this._content.splice(idx + 1, 0, character);
            }
            else {
                throw new Error(`Cannot insert character ${character.id} after an unexisting character ${characterID}.`);
            }
        }

        return character.id;
    }

    static preInsertAfter(state : TextEditor, characterID : string, character : Character, index? : number) {
        // The character after which to insert must exist
        return (index === null && characterID === null) ||
               (index >= 0 && index <= state._content.length) ||
               state.getCharacterIndex(characterID) !== -1;
    }
    
    static postInsertAfter(state : TextEditor, originalState : TextEditor, characterID : string, character : Character, index? : number) {
        /*
         * The character we inserted must occur after the character it was inteded to be.
         * It must not occur right after it since concurrent insertions after the same element are possible.
         */

        return state.getCharacter(character.id).visible &&
               state.getCharacterIndex(characterID) < state.getCharacterIndex(character.id);
    }
    
    delete(characterID) {
        var idx = this.getCharacterIndex(characterID);
        if (idx != -1) {
            // Found
            this._content.splice(idx, 1);
        }
        else {
            throw new Error(`Cannot delete unexisting character: ${characterID}.`);
        }

        /*
        var character = this.getCharacter(characterID);
        if (!character) { throw new Error(`Cannot delete an unexisting character.`); }
        //console.log("deleted character: " + characterID);
        character.visible = false; // mark the character as deleted
        */
    }
    
    static postDelete(state : TextEditor, originalState : TextEditor, characterID : string) {
        /*
        var character = state.getCharacter(characterID);
        return !character.visible;
        */

        return state.getCharacterIndex(characterID) === -1;
    }

    getSize(): number {
        return this._content.length;
    }
    
    getContent(): string {
        /*
        var content = "";
        this._content.forEach(char => {
            char = char.get();
            if (char.visible)
                content += char.char;
        });
        return content; */

        return this._content.reduce((str, char) => {
            if (char.visible)
                return str + char.char;
            else
                return str; 
        }, "");
    }

    getRawContent(): Array<Character> {
        /*
        const content = [];
        this._content.forEach(char => {
            if (char.visible)
                content.push(char);
        });
        return content;
        */

        return this._content.reduce((acc, char) => {
            if (char.visible)
                return acc.concat([char]);
            else
                return acc;
        }, []);
    }
    
    // Wipes out all text contained by the document
    clearDocument() {
        /* Mark all characters as deleted.
           Since characters are passed as arguments they are immutable (i.e. frozen).
           Thus, in order to modify them we will need to serialize and deserialize them. */
        /*
        this._content = this._content.map(c => {
            var mutableCopy = JSON.parse(JSON.stringify(c));
            mutableCopy.visible = false;
            return Object.freeze(mutableCopy);
        }); */
        this._content = [];
    }
    
    static postClearDocument(state : TextEditor, originalState : TextEditor) {
        // Check that all elements from the original state are now invisible
        for (var char of originalState._content) {
            //if (state.getCharacter(char.id).visible) {
                // Element is still visible
            if (state.getCharacterIndex(char.id) !== -1) {
                // Character is still in the document
                return false;
            }
        }
        return true;
    }
    
    getCharacterIndex(id) {
        return this._content.findIndex(c => c.id === id);
    }
    
    getCharacter(id) {
        return this._content.find(c => c.id === id);
    }
    
    tojson() {
        return this._content;
    }
    
    static fromjson(content) {
        var editor = new TextEditor();
        editor._content = content;
        return editor;
    }
}

fw.Factory.registerCmRDTClass(TextEditor);
//fw.Factory.registerExchangeableClass(LinkedList);

export {Character, TextEditor, fw};