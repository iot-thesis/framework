import * as fw from '../../../framework';

const peer1String = 'Peer 1';
const peer2String = 'Peer 2';

fw.Service.subscribe('Number', service => {
    // Access some eventually consistent fields
    console.log(`service.set.has('${peer1String}') = ${service.set.has(peer1String)}`); // true
    service.set.add(peer1String);
    service.set.add(peer2String);
    
    setTimeout(() => {
        console.log(`service.set.has('${peer1String}') = ${service.set.has(peer1String)}`); // true
        service.set.delete(peer1String);
        console.log(`service.set.has('${peer1String}') = ${service.set.has(peer1String)}`); // false
    }, 10000);
});