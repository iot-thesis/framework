import * as fw from '../../../framework';

const peer1String = 'Peer 1';
const peer2String = 'Peer 2';

class SetService extends fw.Service {
    set : ORSet<string>;
    
    constructor() {
        let typetag : string = 'Number';
        super(typetag);
        
        var orSet = new fw.CRDTs.ORSet();
        orSet.add(peer1String);
        
        this.set = super.makeEC('set', orSet);
    }
}

var myService = new SetService();
myService.publish();

console.log(`myService.set.has('${peer1String}') : ${myService.set.has(peer1String)}`); // true
console.log(`myService.set.has('${peer2String}') : ${myService.set.has(peer2String)}`); // false

// After 3 seconds, check again to see if peer2String is now known
setTimeout(() => {
    myService.set.delete(peer1String); // delete it
    console.log(`myService.set.has('${peer1String}') : ${myService.set.has(peer1String)}`); // true since peer 2 also added it
    console.log(`myService.set.has('${peer2String}') : ${myService.set.has(peer2String)}`); // true
}, 3000);