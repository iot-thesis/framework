import * as fw from '../../../framework';

fw.Service.subscribe('GONum', service => {
    console.log(`Discovered service`);
    var number = service.getNumber().num;
    console.log(`The number is ${number}`);
    console.log(`Assigned number to ${service.getNumber().assign(number + 5)}`);
});