import * as fw from '../../../src/framework';

/*
 * Example usage of the JSON CRDT datatype.
 * Implementation of a grow only number
 * that can be assigned but may never decrease.
 */

class GrowOnlyNumber implements CmRDT_I<GrowOnlyNumber> {
    public num : number;
    //public testObj : any;
    constructor(n) {
        this.num = n;
    }
    
    getNum() {
        return this.num;
    }
    
    assign(n) { //, obj) {
        this.num = n;

        // Do some expensive computation to make the benchmarks last longer
        //var tans = [];
        //for (var i = 0; i < 10000; i++)
        //    tans.push(Math.tan(n + i));

        //this.testObj = obj;
        return tans; //this.num;
    }
    
    static preAssign(state, arg) {
        return arg >= state.num;
    }
    
    tojson() {
        return this.num;
    }
    
    static fromjson(n): GrowOnlyNumber {
        return new GrowOnlyNumber(n);
    }
}

fw.Factory.registerCmRDTClass(GrowOnlyNumber);

export default class GONumberService extends fw.Service {
    public number;
    constructor(n) {
        super();
        this.number = super.makeReplica('number', new GrowOnlyNumber(n));
    }

    getReplica() {
        return this.number;
    }

    getNumber() {
        return this.number.getNum();
    }
}

//const service = new GONumberService(0);

//export default service = new GONumberService(0);

//export default service = new GONumberService(0);

///////////////
/*
class TTT implements Exchangeable<TTT> {
    public w : any;
    constructor(val) {
        this.w = val;
    }
    
    tojson() {
        return this.w;
    }
    
    static fromjson(json) {
        return new TTT(json);
    }
}

fw.Factory.registerExchangeableClass(TTT);

var bO = {a:5};
//var badObj = new TTT(bO);
service.number.__addProperty__('dynamic', bO); //badObj);
console.log(`service.number.dynamic.w.a is: ${service.number.dynamic.a}`);

bO.a = 10;

console.log(`service.number.dynamic.w.a is: ${service.number.dynamic.a}`);

var bbb = {b:500};
//var badd = new TTT(bbb); 
service.number.assign(9, bbb); // badd);
console.log(`service.number.testObj.b is: ${service.number.testObj.b}`);

bbb.b = 999;

console.log(`service.number.testObj.b is: ${service.number.testObj.b}`);
*/
//////////////

/*
service.number.__metaObject__.onRemoteUpdate(numb => {
    console.log(`Number is now ${numb.getNum()}`);
    if (numb.testProp) {
        console.log(`numb.testProp = ${numb.testProp}`);
    }
});

service.number.assign(10);
service.number.assign(10);    //[10] [12] [15, 20, 25, 30, 13, 28, 29]
service.number.assign(12);

service.publish('GONum');
console.log(`binnen 10 seconden operaties`);   //  10 --> 1,  10 --> 2, 12 --> 3, 15 --> 4, 20 --> 5, 25 --> 6, 30 --> 7

setTimeout(() => {
    service.number.assign(15);
    service.number.assign(20);
    service.number.assign(25);
    service.number.assign(30);
    service.number.__addProperty__('testProp', 142);
    
    console.log('operaties done');
}, 15000); */