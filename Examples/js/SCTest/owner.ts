import * as fw from '../../../src/framework';
//const fw = require('./Fw/compiled/src/framework').default;

class SCService extends fw.Service {
    constructor(number) {
        super();

        this.scField = super.makeReplica('scField', { val: number,
            plus: function(x) {
                console.log(`${this.val} + ${x} = ${this.val + x}`);
                this.val += x;
                return this.val;
            },
            minus: function(x) {
                console.log(`${this.val} - ${x} = ${this.val - x}`);
                this.val -= x;
                return this.val;
            }
        });
    }
}

var s = new SCService(5);
s.publish('StrongService');
s.scField.plus(9);
s.scField.plus(9);