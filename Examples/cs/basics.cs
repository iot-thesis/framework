service Temperature {
	rep temp = 5;
    rep arr  = new GMap(); //EArray.from([5,10,20]);
    far hum  = 30;
}

var tempService = new Temperature();
//console.log('arr[1] = ' + tempService.arr[1]);

publish tempService as 'TEMP'
subscribe 'TEMP' with function(s) { console.log('Temp is: ' + s.temp); }