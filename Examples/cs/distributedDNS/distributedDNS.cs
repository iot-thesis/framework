// Distributed Domain Name Service
service DNS {
    rep map = new GMap();
    
    add(url, domainInfo) {
        this.map.set(url, domainInfo);
    }
    
    resolve(url) {
        return this.map.get(url);
    }
}

var dnsService = new DNS();

// Exchangeable class
class DomainNameInfo {
    constructor(name, ip, address) {
        this.name = name;
        this.ip = ip
        this.address = address;
    }
    
    equals(replica) {
        return replica.ip === this.ip;
    }

    tojson() {
        return { name: this.name, ip: this.ip, address: this.address };
    }

    static fromjson(json) {
        return new DomainNameInfo(json.name, json.ip, json.address);
    }
}
Factory.registerExchangeableClass(DomainNameInfo);

var vubInfo = new DomainNameInfo('Vrije Universiteit Brussel', '134.184.129.2', 'Pleinlaan 9');
dnsService.add('vub.ac.be', vubInfo);

// Try to fiddle with the nested object
console.log("ip is: " + dnsService.resolve('vub.ac.be').ip);

var info = dnsService.map.get('vub.ac.be'); // is a deep copy
info.ip = "new"; // modifies the copy, not the object that is nested within the CRDT

console.log("ip is: " + dnsService.resolve('vub.ac.be').ip);