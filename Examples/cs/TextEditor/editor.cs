/* 
 * Run this server:
 * $ ts-node server
 * Then open 'textEditor.html' in the browser.
 */

var express = require('express');
var path    = require('path');
var app     = express();
var ip      = require('ip');

/*
 * Serve the text editor as a static file.
 */

app.use(express.static(path.join(__dirname)));
app.use('/images', express.static(path.join(__dirname, '..', 'images')));

/*
 * Create a TextEditor service and publish it.
 */

import {TextEditor, Character} from './logic';
deftype TextEditor

var editorService;
subscribe TextEditor with editor => {
    console.log('Received text editor service. You can now start editing the document');
    editorService = editor;
    editor.textEditor.onUpdate(pushUpdate);
};

/*
 * Initialize websocket
 */

var WebSocketServer = require('ws').Server,
    port = 8080,
    wss = new WebSocketServer({port: port});

console.log('Websocket server listening at http://127.0.0.1:' + port);

/*
 * Listen for connections and messages
 */

var socket;
var buffer = [];
wss.on('connection', function(sock) {
    socket = sock;
    onSocket(sock);
    socket.on('message', function(msg) {
        msg = JSON.parse(msg);
        if (!msg.operation || !msg.args)
            console.log('Did not understand message coming from the front-end.');
        else
            onMessage(msg);
    });
    
    socket.on('close', function() {
        socket = null;
    });
    
    socket.on('error', function(error) {
        console.log('Socket error: ' + error);
        socket = null;
    });
});

function onSocket(s) {
    socket = s;
    flushBuffer();
}

function flushBuffer() {
    buffer.forEach(sendMessage);
    buffer = [];
}

// Pushes the new document content to the front-end
function pushUpdate(editor) {
    var msg = { type: 'documentUpdate', content: editor._content };
    sendMessage(msg);
}

/*
 * Processes received messages
 */

function onMessage({operation, args}) {
    switch (operation) {
        case 'insertAfter':
            var [previousCharacterID, addedCharacter] = args;
            var newCharacter = new Character(addedCharacter);
            editorService.textEditor.insertAfter(previousCharacterID, newCharacter);
            break;
        case 'delete':
            var [characterID] = args;
            editorService.textEditor.delete(characterID);
            break;
        case 'clearDocument':
            editorService.textEditor.clearDocument();
            break;
        default:
            console.log('Unsupported operation \'' + operation + '\'');
    }
}

function sendMessage(msg) {
    if (socket)
        socket.send(JSON.stringify(msg));
    else
        buffer.push(msg);
}