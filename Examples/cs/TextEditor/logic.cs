/*
 * Implementation of a collaborative text editor as a JSON CRDT.
 * A text document is a sequence of characters.
 *
 * Supported operations:
 *  - insertAfter(characID, charac) --> characID
 *  - delete(characID) --> void
 *  - clearDocument()  --> void
 *  - getContent() --> string
 */

//import * as fw from '../../../framework';

// Representation of a character in a text document
var characterCounter = 1;
class Character {
    constructor(c, id, visible) { //id = (fw.ipAddress + ':' + characterCounter++), visible = true) {
        if (c.length !== 1) { throw new TypeError('Expected a character, but got a string with a length that is not 1.'); }
        this.char    = c;
        this.visible = visible ? visible : true;
        this.id      = id ? id : (fw.ipAddress + ':' + characterCounter++);
    }
}

/*
 * `TextEditor` class is a General Purpose CRDT.
 * Therefore, it implements the `CmRDT_I<TextEditor>` interface.
 */
class TextEditor {
    constructor() {
        this._content = [];
    }
    
    insertAfter(characterID, character) {
        character = JSON.parse(JSON.stringify(character)); // Because the received arguments are immutable!
        if (!characterID) {
            // Insert at the beginning of the document
            this._content.splice(0, 0, character);
        }
        else {
            if (this.getCharacter(character.id)) {
                throw new Error('Characters must be unique, cannot insert the same character multiple times.');
            }
            
            var idx = this.getCharacterIndex(characterID);
            if (idx != -1) {
                // Found
                this._content.splice(idx + 1, 0, character);
            }
            else {
                throw new Error('Cannot insert character ' + character.id + ' after an unexisting character ' + characterID + '.');
            }
        }

        return character.id;
    }
    
    post insertAfter(state, originalState, characterID, character) {
        /*
         * The character we inserted must occur after the character it was inteded to be.
         * It must not occur right after it since concurrent insertions after the same element are possible.
         */
        
        return state.getCharacter(character.id).visible &&
               state.getCharacterIndex(characterID) < state.getCharacterIndex(character.id);
    }
    
    delete(characterID) {
        var character = this.getCharacter(characterID);
        if (!character) { throw new Error('Cannot delete an unexisting character.'); }
        character.visible = false; // mark the character as deleted
    }
    
    post delete(state, originalState, characterID) {
        var character = state.getCharacter(characterID);
        return !character.visible;
    }
    
    getContent() {
        return this._content.reduce((str, char) => {
            if (char.visible)
                return str + char.char;
            else
                return str; 
        }, "");
    }
    
    // Wipes out all text contained by the document
    clearDocument() {
        /* Mark all characters as deleted.
           Since characters are passed as arguments they are immutable (i.e. frozen).
           Thus, in order to modify them we will need to serialize and deserialize them. */
        this._content = this._content.map(c => { 
            var mutableCopy = JSON.parse(JSON.stringify(c));
            mutableCopy.visible = false;
            return Object.freeze(mutableCopy);
        });
    }
    
    post clearDocument(state, originalState) {
        // Check that all elements from the original state are now invisible
        for (var char of originalState._content) {
            if (state.getCharacter(char.id).visible) {
                // Element is still visible
                return false;
            }
        }
        return true;
    }
    
    getCharacterIndex(id) {
        return this._content.findIndex(c => c.id === id);
    }
    
    getCharacter(id) {
        return this._content.find(c => c.id === id);
    }
    
    tojson() {
        return this._content;
    }
    
    static fromjson(content) {
        var editor = new TextEditor();
        editor._content = content;
        return editor;
    }
}

Factory.registerCmRDTClass(TextEditor);

export {Character, TextEditor}; //, fw};