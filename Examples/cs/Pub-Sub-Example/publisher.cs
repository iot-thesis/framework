/*
 * Example illustrating how to use the DSL to publish and subscribe to services.
 * When being in this folder use `../../../run.sh ../../../ publisher.cs ./compiled` to run this file.
 */

class WrappedString {
    constructor(test) {
        this._test = test;
    }
    
    unwrap() {
        return this._test;
    }
    
    equals(replica) {
        return this.unwrap() === replica.unwrap();
    }
    
    toJSON() {
        return this._test;
    }
    
    fromJSON(json) {
        return new WrappedString(json);
    }
}

Factory.registerExchangeableClass(WrappedString);

// Define the service that contains eventually and strongly consistent replicas.
service NumberService {
    makeMap() {
        var m   = new GMap();
        var arr = new EArray();
        arr.push(new WrappedString("A"), new WrappedString("B"), new WrappedString("C"));
        
        m.set('a', arr);
        m.set('prim', 5);
        return m;
    }
    
    weak map = this.makeMap(); // first argument must equal field name!
    strong strong = { val: 5,
                      plus: function(x) {
                          this.val += x;
                          return this.val;
                      },
                      minus: function(x) {
                          this.val -= x;
                          return this.val;
                      }
                    };
}

var myService = new NumberService();
publish myService as 'Number'

console.log(myService.map);
console.log('myService.map.get("a") : ' + myService.map.get('a')[0].unwrap());
console.log('myService.map.get("b") : ' + myService.map.get('b')); // will be undefined

// After 3 seconds, check again to see if key 'b' is now known
setTimeout(() => {
    if (myService.map.has('b')) {
        console.log('myService.map.get("a") : ' + myService.map.get('a')[0].unwrap());
        console.log('myService.map.get("b") : ' + myService.map.get('b').d.unwrap());
        myService.map.set('c', new WrappedString('valueC')); // add a new key
        console.log(myService.map);
    }
    else {
        console.log('We did not discover the subscriber, check the multicast settings.');
    }
}, 3000);