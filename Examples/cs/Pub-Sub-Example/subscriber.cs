class WrappedString {
    constructor(test) {
        this._test = test;
    }
    
    unwrap() {
        return this._test;
    }
    
    equals(replica) {
        return this.unwrap() === replica.unwrap();
    }
    
    toJSON() {
        return this._test;
    }
    
    fromJSON(json) {
        return new WrappedString(json);
    }
}

Factory.registerExchangeableClass(WrappedString);

subscribe 'Number' with function(service) {
    var obj = EObject.create({ d: new WrappedString("D"), e: new WrappedString("E"), f: new WrappedString("F") });
    service.map.set('b', obj);

    // Access some eventually consistent fields
    console.log('service.map.get("a") = ' + service.map.get('a')[0].unwrap());
    console.log('service.map.get("prim") = ' +service.map.get('prim'));
    console.log('service.map.get("b") = ' + service.map.get('b').d.unwrap());
    
    // RPC some strongly consistent fields
    service.strong
           .then(obj => obj.val)
           .then(val => console.log('service.strong.val: ' + val));
    
    service.strong
           .then(obj => obj.plus(3))
           .then(val => console.log('service.strong.plus(3): ' + val));
    
    service.strong
           .then(obj => obj.val)
           .then(val => console.log('service.strong.val: ' + val));

    // After 10 seconds, check if key 'c' is now known
    setTimeout(() => {
        if (service.map.has('c')) {
            console.log('service.map.get("c") = ' + service.map.get('c').unwrap());   
        }
    }, 10000);
}