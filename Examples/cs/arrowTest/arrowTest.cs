import { service, publish, subscribe, deftype, class, <= } from '../../../transpiler.sjs';
var a = { b: 5,
          f: (x,y) => {
            return { res: x+y }
          }
};
//(a <= b).then(c => { console.log(c); });
(a<=f(3,4)).then(resObj => (resObj <= res))
          .then(res => console.log(res));