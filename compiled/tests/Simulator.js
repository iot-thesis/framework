Object.defineProperty(exports, "__esModule", { value: true });
const fw = require("../src/framework");
exports.fw = fw;
const serialization_1 = require("../src/Application/serialization");
const Service = fw.Service;
function broadcastOperation(peer, serviceID, replicaName, operation) {
    peer.broadcastOperation(serviceID, replicaName, operation);
}
var Connectivity;
(function (Connectivity) {
    Connectivity[Connectivity["ONLINE"] = 0] = "ONLINE";
    Connectivity[Connectivity["OFFLINE"] = 1] = "OFFLINE";
})(Connectivity || (Connectivity = {}));
class Peer {
    constructor(id = Peer.ctr++) {
        this.id = id;
        this.services = new Map();
        this.publications = new Set();
        this.subscriptions = new Map();
        this.connectivityStatus = Connectivity.OFFLINE;
        this.buffer = [];
        Peer.peers.push(this);
    }
    // Adds a service on the peer
    addService(name, service) {
        if (!(service instanceof Service))
            throw new TypeError("Argument to `addService` is not a service.");
        this.services.set(name, service);
        return service;
    }
    // Publishes the service with the given name under the given typetag
    publish(name, typetag) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            if (!this.services.has(name))
                throw new Error(`Peer ${this.id} has no service named ${name}.`);
            const service = this.services.get(name);
            if (this.publications.has(service))
                return; // already published
            if (Peer.publications.has(typetag)) {
                var setOfPublications = Peer.publications.get(typetag);
                setOfPublications.add(service);
            }
            else {
                Peer.publications.set(typetag, new Set([service]));
            }
            // Send this service to all peers that subscribed to it
            if (Peer.subscriptions.has(typetag)) {
                const setOfPeers = Peer.subscriptions.get(typetag);
                setOfPeers.forEach(peer => peer.recvService(service, typetag));
            }
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.publish(name, typetag));
        }
    }
    subscribe(typetag, onPublication) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            if (this.subscriptions.has(typetag))
                return; // already subscribed
            if (Peer.subscriptions.has(typetag)) {
                const setOfPeers = Peer.subscriptions.get(typetag);
                setOfPeers.add(this);
            }
            else {
                Peer.subscriptions.set(typetag, new Set([this]));
            }
            // Check services that were already published
            if (Peer.publications.has(typetag)) {
                this.subscriptions.set(typetag, onPublication);
                const setOfServices = Peer.publications.get(typetag);
                setOfServices.forEach(service => {
                    const serviceCopy = service; //Service.fromString(service.toString());
                    this.recvService(serviceCopy, typetag);
                    //onPublication(serviceCopy);
                });
            }
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.subscribe(typetag, onPublication));
        }
    }
    // Receives a service
    recvService(service, typetag) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            if (this.subscriptions.has(typetag)) {
                const serviceCopy = Service.fromString(service.toString());
                this.services.set(service._ID_, serviceCopy); // also store received services (under some unique identifier)
                const onPublication = this.subscriptions.get(typetag);
                onPublication(serviceCopy);
            }
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.recvService(service, typetag));
        }
    }
    // Delivers the operation to the correct service
    receiveOperation(msg) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            const { serviceID, name, op } = JSON.parse(msg), operation = serialization_1.deserialize(op);
            this.services.forEach(service => {
                if (service._ID_ === serviceID) {
                    service._REPLICAS_[name].receive(operation);
                }
            });
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.receiveOperation(msg));
        }
    }
    broadcastOperation(serviceID, replicaName, operation) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            var infoMessage = JSON.stringify({ serviceID: serviceID, name: replicaName, op: serialization_1.serialize(operation) });
            Peer.peers.forEach(peer => {
                if (peer.id !== this.id)
                    peer.receiveOperation(infoMessage);
            });
        }
        else {
            this.buffer.push(() => this.broadcastOperation(serviceID, replicaName, operation));
        }
    }
    // Makes the peer go online (will execute all lambda's stored in `buffer`)
    goOnline() {
        this.connectivityStatus = Connectivity.ONLINE;
        this.buffer.forEach(action => action());
        this.buffer = [];
    }
    goOffline() {
        this.connectivityStatus = Connectivity.OFFLINE;
    }
}
Peer.ctr = 1; // static peer counter
Peer.publications = new Map(); // Map<TypeTag, Set<Service>>
Peer.subscriptions = new Map(); // Map<TypeTag, Set<Peer>>
Peer.peers = [];
// Proxies
const peerHandler = {
    get(target, prop, receiver) {
        fw.Config.setBroadcastMechanism(target.broadcastOperation.bind(target));
        if (target.hasOwnProperty(prop) || typeof target[prop] === 'function')
            return target[prop];
        else
            return target.services.get(prop);
    }
};
const PeerProxy = new Proxy(Peer, {
    construct(target, args) {
        const peer = new target(...args);
        // Return a proxy on the peer
        return new Proxy(peer, peerHandler);
    }
});
exports.Peer = PeerProxy;
//# sourceMappingURL=Simulator.js.map