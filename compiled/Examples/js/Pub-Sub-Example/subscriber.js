Object.defineProperty(exports, "__esModule", { value: true });
const fw = require("../../../src/framework");
function WrappedString(test) {
    this._test = test;
    this.unwrap = function () {
        return this._test;
    };
    this.equals = function (replica) {
        return this.unwrap() === replica.unwrap();
    };
}
fw.Service.subscribe('Number', service => {
    //var arr = fw.Exchangeables.EArray.from([new WrappedString("D"), new WrappedString("E"), new WrappedString("F")]);
    //var obj = fw.Datastructures.Exchangeables.EObject.create({ d: new WrappedString("D"), e: new WrappedString("E"), f: new WrappedString("F") });
    service.map.set('b', new WrappedString('valueB'));
    // Access some eventually consistent fields
    console.log(`service.map.get('a') = ${service.map.get('a')[0].unwrap()}`);
    console.log(`service.map.get('prim') = ${service.map.get('prim')}`);
    //console.log(`service.map.get('b') = ${service.map.get('b').d.unwrap()}`);
    // RPC some strongly consistent fields
    service.strong
        .then(obj => obj.val)
        .then(val => console.log(`service.strong.val: ${val}`));
    service.strong
        .then(obj => obj.plus(3))
        .then(val => console.log(`service.strong.plus(3): ${val}`));
    service.strong
        .then(obj => obj.val)
        .then(val => console.log(`service.strong.val: ${val}`));
    service.map.onUpdate(map => {
        if (map.has('c')) {
            console.log(`map.get('c') = ${map.get('c').unwrap()}`);
        }
        else {
            console.log(`Publisher did not add key 'c'.`);
        }
    });
});
//# sourceMappingURL=subscriber.js.map