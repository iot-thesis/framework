Object.defineProperty(exports, "__esModule", { value: true });
const fw = require("../../../src/framework");
/*
 * Example illustrating how to use the `GMap` CRDT together with user-defined (i.e. non-primitive) types.
 */
/*
class WrappedString implements Eq<WrappedString> {
    public _test: string;
    constructor(test : string) {
        this._test = test;
    }
    
    unwrap() {
        return this._test;
    }
    
    equals(replica : WrappedString): boolean {
        return this.unwrap() === replica.unwrap();
    }
    
    tojson(): any {
        return this._test;
    }
    
    static fromjson(json : any): WrappedString {
        return new WrappedString(json);
    }
} */
function WrappedString(test) {
    this._test = test;
    this.unwrap = function () {
        return this._test;
    };
    this.equals = function (replica) {
        return this.unwrap() === replica.unwrap();
    };
}
//fw.Factory.registerExchangeableClass(WrappedString);
/*
class O implements CvRDT_I<O> {
    private _num : number;
    constructor() {
        this._num = 5;
    }
    
    set(s : number) {
        this._num = s;
    }
    
    getNum() {
        return this._num;
    }
    
    merge(replica : O) {
        this._num = Math.max(replica.getNum(), this._num);
    }

    equals(replica : O): boolean {
        return this.getNum() === replica.getNum();
    }
    
    toJSON() {
        return this.getNum();
    }
    
    static fromJSON(num) {
        var o = new O();
        o.set(num);
        return o;
    }
}

fw.Factory.registerCvRDTClass(O);
*/
var wsA = new WrappedString("A");
// Define the service that contains eventually and strongly consistent replicas.
class NumberService extends fw.Service {
    //test: any;
    constructor(number) {
        super();
        // GMap, ORSet, LWWSet, SECRO moeten nog immutability nagaan!
        /* (we hebben service, replica en util aangepast)
         
           Het probleem is dat CRDTs intern een aantal objecten gebruiken wss (vb added set, removed set, etc).
           En natuurlijk moeten die niet immutable zijn.
           
           Dus ofwel --> Elke CRDT moet nagaan dat user argument immutable is.
                         Maar dan zullen user-defined CRDTs dat ook moeten checken, kunnen wij niet nagaan..
                         
               ofwel --> Elke CRDT moet zijn internals declareren, of wij checken welke properties internal zijn.
                         En non-internals moeten immutable zijn (maar nog steeds probleem, user-input zal wss in één van die
                         internal objectjes terechtkomen, en kunnen wij dus nooit checken...)
                         
           Oplossing: In util code: wanneer het een CRDT is, skippen, indien het een object is checken + recursief checken
               --> In __addProperty__ checken dat de waarde immutable is (dus util functie)  OK
               --> In de CRDTs zelf de user-input checken (in GMap, ORSet, LWWSet & SECRO)
               --> CRDTs: in de fromJSON, opnieuw immutable maken --> is beter in (de)serialize dan in fromJSON... !
               
           MSS BETER VOOR IMMUTABILITY:
               --> Overal waar iets immutable verwacht wordt --> nagaan dat het een type is uit immutable JS
                   (dus primitive of immutable JS type)
               --> Alle exchangeable types: EArray & EObject immutables maken en de elementen van de array (of values v/d properties van het object) moeten ook immutable zijn!
         */
        var m = new fw.Datastructures.CRDTs.GMap();
        //var o = new O();
        //this.test   = super.makeCvRDT('test', o);
        this.map = super.makeReplica('map', m); // first argument must equal field name!
        this.strong = super.makeReplica('strong', { val: number,
            plus: function (x) {
                this.val += x;
                return this.val;
            },
            minus: function (x) {
                this.val -= x;
                return this.val;
            }
        });
    }
}
var myService = new NumberService(5);
//var arr = new fw.Datastructures.Exchangeables.EArray();
//arr.push(wsA, new WrappedString("B"), new WrappedString("C"));
console.log('wsA');
console.log(wsA);
var arr = [wsA, new WrappedString("B"), new WrappedString("C")];
/* --- Voor EObject test ---
var arr = new fw.Datastructures.Exchangeables.EObject();
arr.addProperty('testProp', 6);
arr.addProperty('otherProp', wsA);
*/
myService.map.set('a', arr); //new WrappedString("valueA"));
myService.map.set('prim', 5);
// mss is aan metaObject gaan en dan aan baseObject en dan zien hoe we aan die 'a' geraken
// ik denk dat de 'a' die erin zit de plain JS 'a' is.
//console.log(myService.map);
/*
console.log(`myService.map.get('a')[0].unwrap() = ${myService.map.get('a').testProp}`);
console.log(`myService.map.get('a')[0].unwrap() = ${myService.map.get('a').otherProp.unwrap()}`);
wsA._test = "B";
console.log(`myService.map.get('a')[0].unwrap() = ${myService.map.get('a').testProp}`);
console.log(`myService.map.get('a')[0].unwrap() = ${myService.map.get('a').otherProp.unwrap()}`);
*/
console.log(myService.map);
console.log(myService.map.get('a'));
console.log(`myService.map.get('a')[0].unwrap() = ${myService.map.get('a')[0].unwrap()}`);
wsA._test = "B";
console.log(`myService.map.get('a')[0].unwrap() = ${myService.map.get('a')[0].unwrap()}`);
/*
myService.test.__metaObject__.onUpdate((o) => {
    console.log(`Number changed: ${o.getNum()}`);
}); */
/*
myService.test.set(6);
myService.test.set(7);
myService.test.set(7);
myService.test.set(9);
*/
myService.publish('Number');
console.log(myService.map);
console.log(`myService.map.get('a') : ${myService.map.get('a')[0].unwrap()}`);
console.log(`myService.map.get('b') : ${myService.map.get('b')}`); // will be undefined
myService.map.onRemoteUpdate((map) => {
    if (map.has('b')) {
        console.log('-------|------');
        console.log(map);
        console.log('-------|------');
        console.log(`myService.map.get('a') : ${myService.map.get('a')[0].unwrap()}`);
        console.log(`myService.map.get('b') : ${myService.map.get('b').unwrap()}`);
        myService.map.set('c', new WrappedString('valueC')); // add a new key
        console.log(myService.map);
    }
    else {
        console.log('Subscriber did not add key \'b\'.');
    }
});
//# sourceMappingURL=publisher.js.map