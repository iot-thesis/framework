/*
 * Run this editor:
 * $ ../../../run.sh editor.js
 * Then open 'textEditor.html' in the browser.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("./util");
/*
 * Subscribe to text editor services.
 */
const treeLogic_1 = require("./treeLogic");
var editorService = null;
treeLogic_1.fw.Service.subscribe('TextEditor', editor => {
    if (editorService === null) {
        util_1.init(editor);
        console.log(`Received text editor service. You can now start editing the document`);
        editorService = editor;
        editor.textEditor.onUpdate(util_1.pushUpdate);
    }
    else {
        console.log('[WARNING]: Already found a text editor service. Ignoring this one.');
    }
});
//# sourceMappingURL=editor.js.map