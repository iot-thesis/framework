/*
 * Implementation of a tree-based collaborative text editor using SECROs.
 * A text document is a balanced tree of characters.
 *
 * Supported operations:
 *  - insertAfter(pos|null, charac) --> pos
 *  - delete(pos) --> void
 *  - clearDocument()  --> void
 *  - getContent() --> string
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fw = require("../../../src/framework");
exports.fw = fw;
const accessor = fw.Decorators.accessor;
require('google-closure-library');
goog.require('goog.structs.AvlTree');
// A `Character` is a tuple: (char, pos)
class Character {
    constructor(char, pos) {
        this.char = char;
        this.pos = pos;
    }
    // Compares the positions of 2 characters.
    // Returns 0 for equal positions,
    // a negative number if this position is smaller,
    // a positive number if this position is bigger.
    static compare(char1, char2) {
        return char1.pos - char2.pos;
    }
}
exports.Character = Character;
class TextEditor {
    constructor(str = "") {
        this._docTree = new goog.structs.AvlTree(Character.compare);
        var pos = null;
        for (var i = 0; i < str.length; i++)
            pos = this.insertAfter(pos, str.charAt(i));
    }
    // Returns a boolean indicating the presence or absence of the given position in the document.
    hasPosition(pos) {
        const dummyChar = { char: '', pos: pos };
        return this._docTree.contains(dummyChar);
    }
    /*
     * Inserts a character after a certain position.
     * Returns the position of the newly inserted character,
     * which is defined as:
     *  position(c) = 1 iff document is empty
     *                position(first(doc)) / 2 iff the character is prepended to the document
     *                pos+1 iff the position after which to insert is the last position in the document
     *                [ pos + position(next(pos)) ] / 2  iff the character is inserted in between two other positions.
     */
    insertAfter(pos, char) {
        if (char.length !== 1)
            throw new TypeError(`Expected a character, but got a string with a length that is not 1.`);
        const newPos = this.determinePosition(pos), newChar = new Character(char, newPos);
        if (!this._docTree.add(newChar))
            throw new Error(`Insertion failed.`);
        else
            return newPos;
    }
    static preInsertAfter(state, pos, char) {
        // The position after which to insert must exist
        return pos === null || state.hasPosition(pos);
    }
    static postInsertAfter(state, originalState, args, iPos) {
        /*
         * The character we inserted must occur after the character it was inteded to be.
         * It must not occur right after it since concurrent insertions after the same element are possible.
         */
        const [pos, char] = args;
        const originalChar = { char: "dummy", pos: pos }, insertedChar = { char: "dummy", pos: iPos };
        // state eens printen
        console.log("state is:");
        console.log(state);
        return (pos === null && state._docTree.contains(insertedChar)) ||
            state._docTree.indexOf(originalChar) < state._docTree.indexOf(insertedChar);
    }
    // Computes a new position that is bigger than `after` and smaller than `next(after)`.
    determinePosition(after) {
        if (after === null) {
            // Prepend `char` to the document
            if (this._docTree.getCount() === 0) {
                // Document is empty
                return 1;
            }
            else {
                const first = this._docTree.getMinimum(); // character with the smallest position (i.e. the first character)
                return first.pos / 2;
            }
        }
        else {
            var next = null;
            // The AVL tree's API does not allow us to get a node and then ask for its next node.
            // Therefore we could use `indexOf` to determine the index of `after` and then ask for
            // the node at index `after+1` using `getKthValue`.
            // However, this would be in O(2*log(n)), so it's more efficient to use a traversal approach
            // starting at `after` and only going to it's next element, which is in `O(log(n) + 1)`.
            this._docTree.inOrderTraverse(char => {
                next = char.pos;
                return next !== after; // stops the traversal when we are at the successor of `after`.
            }, new Character("dummy", after));
            if (next === null) {
                // `after` does not exist in the document!
                throw new Error(`Unexisting position: ${after}.`);
            }
            else if (next !== after) {
                // we will insert in between `after` and `next`
                return (after + next) / 2;
            }
            else {
                // next === after
                // `after` is the last position in the document.
                // We will append `char` to the document.
                return after + 1;
            }
        }
    }
    delete(pos) {
        if (this._docTree.remove(pos) === null)
            throw new Error(`Position does not exist: ${pos}.`);
    }
    static postDelete(state, originalState, args) {
        // Position must be deleted!
        const [pos] = args;
        return !state.hasPosition(pos);
    }
    getSize() {
        return this._docTree.getCount();
    }
    getContent() {
        var str = "";
        this._docTree.inOrderTraverse(char => {
            str += char.char;
            return false;
        });
        return str;
    }
    getRawContent() {
        var acc = [];
        this._docTree.inOrderTraverse(char => {
            acc.push(char);
            return false;
        });
        return acc;
    }
    tojson() {
        return this.getRawContent();
    }
    static fromjson(content) {
        var editor = new TextEditor();
        content.forEach(char => editor._docTree.add(char));
        return editor;
    }
}
__decorate([
    accessor
], TextEditor.prototype, "hasPosition", null);
__decorate([
    accessor
], TextEditor.prototype, "determinePosition", null);
exports.TextEditor = TextEditor;
fw.Factory.registerCmRDTClass(TextEditor);
//# sourceMappingURL=treeLogic.js.map