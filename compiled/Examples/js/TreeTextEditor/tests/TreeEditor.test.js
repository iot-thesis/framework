Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const sim = require("../../../../tests/Simulator");
const treeLogic_1 = require("../treeLogic");
const fw = sim.fw;
fw.Factory.registerCmRDTClass(treeLogic_1.TextEditor);
class Doc extends fw.Service {
    constructor(initialContent) {
        super();
        this.doc = super.makeReplica('doc', new treeLogic_1.TextEditor(initialContent));
    }
}
describe('Testing tree-based collaborative text editor.', () => {
    const peer1 = new sim.Peer(), peer2 = new sim.Peer(), peer3 = new sim.Peer(), typetag = 'TextDocument';
    // Peer 1 creates a service and publishes it
    peer1.addService('docService', new Doc());
    peer1.publish('docService', typetag);
    // Peer 2 & 3 subscribe to the service
    peer2.subscribe(typetag, service => {
        peer2.docService = service;
    });
    peer3.subscribe(typetag, service => {
        peer3.docService = service;
    });
    peer1.goOnline();
    peer2.goOnline();
    peer3.goOnline();
    // Set the IDs of both CmRDTs to different values (because by default the framework takes the ip address)
    // TODO: do this automatically in `addService` and upon receiving a service (`recvService`)
    peer1.docService.doc.__metaObject__._clock._ownID = peer1.id;
    peer2.docService.doc.__metaObject__._clock._ownID = peer2.id;
    peer3.docService.doc.__metaObject__._clock._ownID = peer3.id;
    var posA, posC;
    it('Sequential insertions and deletions.', () => {
        // Do some tests
        posA = peer1.docService.doc.insertAfter(null, 'a');
        const posB = peer2.docService.doc.insertAfter(posA, 'b');
        posC = peer3.docService.doc.insertAfter(posB, 'c');
        peer2.docService.doc.delete(posB);
        assert.equal(peer1.docService.doc.getContent(), peer2.docService.doc.getContent());
        assert.equal(peer1.docService.doc.getContent(), peer3.docService.doc.getContent());
        assert.equal(peer1.docService.doc.getContent(), "ac");
    });
    it('Concurrent insertions and deletions.', () => {
        peer1.goOffline();
        peer2.goOffline();
        peer3.goOffline();
        // Do some tests
        const posD = peer1.docService.doc.insertAfter(null, 'd'); // prepend
        peer1.docService.doc.insertAfter(posC, 'u'); // append
        // concurrent inserts at the same position
        peer2.docService.doc.insertAfter(posA, 'l');
        peer3.docService.doc.insertAfter(posA, 'o');
        // Exchange operations
        peer1.goOnline();
        peer2.goOnline();
        peer3.goOnline();
        assert.equal(peer1.docService.doc.getContent(), peer2.docService.doc.getContent());
        assert.equal(peer1.docService.doc.getContent(), peer3.docService.doc.getContent());
        assert.equal(peer1.docService.doc.getContent(), "dalocu");
    });
});
//# sourceMappingURL=TreeEditor.test.js.map