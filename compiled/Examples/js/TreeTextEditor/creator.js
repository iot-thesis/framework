/*
 * Run this creator:
 * $ ../../../run.sh creator
 * Then open 'textEditor.html' in the browser.
 */
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("./util");
/*
 * Create a TextEditor service and publish it.
 */
const treeLogic_1 = require("./treeLogic");
class TextEditorService extends treeLogic_1.fw.Service {
    constructor(n) {
        super();
        this.textEditor = super.makeReplica('textEditor', new treeLogic_1.TextEditor());
    }
}
var editorService = new TextEditorService();
util_1.init(editorService);
editorService.textEditor.onUpdate(util_1.pushUpdate);
editorService.publish('TextEditor');
//# sourceMappingURL=creator.js.map