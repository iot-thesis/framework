Object.defineProperty(exports, "__esModule", { value: true });
const ip = require('ip');
const ipAddress = ip.address();
exports.ipAddress = ipAddress;
if (ipAddress === '127.0.0.1')
    throw new Error(`No network connection (${ipAddress}). There should be a network connection at startup.`);
/*
 * We avoid importing modules multiple times.
 * Therefore we import them all only once, here.
 * Modules that are dependent on other modules will be initialized with their dependencies.
 */
const jsExtensions = require("./extensions");
const applicationModule = require("./Application/application");
const replicaModule = require("./Application/replica");
const asyncProxyModule = require("./Application/AsyncProxy");
const lanModule = require("./Application/Communication/Plugins/LAN/plugin");
const serviceDiscoveryModule = require("./Application/Communication/serviceDiscovery");
const serviceModule = require("./Application/service");
const operationModule = require("./Application/CRDTs/SECRO/operation");
const CRDTsModule = require("./Application/CRDTs/CRDT-datastructures");
const serializationModule = require("./Application/serialization");
exports.serializationModule = serializationModule;
const vectorClockModule = require("./Application/vector-clock");
const decoratorModule = require("./decorators");
const util = require("./Application/util");
// Initialize modules that have dependencies
lanModule.setDependencies(applicationModule);
// Configure the LAN module
const RPCPort = 8000;
const multicastPort = 8001; //8080;
const multicastAddress = '225.0.0.1';
lanModule.configure(multicastAddress, multicastPort, RPCPort);
serializationModule.setDependencies(CRDTsModule.ExchangeableFactory, util);
operationModule.setDependencies(ipAddress, util);
CRDTsModule.setDependencies(operationModule.Operation, serializationModule, ipAddress, vectorClockModule.VectorClock, util);
jsExtensions.setDependencies(CRDTsModule.ExchangeableFactory, util);
applicationModule.setDependencies(serviceModule, serviceDiscoveryModule, ipAddress);
// Configure the application module
applicationModule.configure(RPCPort);
serviceDiscoveryModule.setDependencies(applicationModule, serviceModule, replicaModule, lanModule, serializationModule, ipAddress, CRDTsModule.SECRO);
serviceModule.setDependencies(replicaModule, serviceDiscoveryModule, lanModule, CRDTsModule, serializationModule, CRDTsModule.SECRO, util, ipAddress);
asyncProxyModule.setDependencies(replicaModule.Replica, util);
replicaModule.setDependencies(serviceModule, CRDTsModule, serviceDiscoveryModule, CRDTsModule.CvRDTFactory, CRDTsModule.CmRDTFactory, asyncProxyModule.AsyncProxy, serializationModule, ipAddress, util);
vectorClockModule.setDependencies(CRDTsModule.ExchangeableFactory, ipAddress);
util.setDependencies(CRDTsModule.Conditions, serializationModule, CRDTsModule.CvRDTFactory, CRDTsModule.CmRDTFactory, replicaModule);
// Polyfill for Object.entries which apparently does not exist in nodeJS
if (!Object.entries)
    Object.entries = function (obj) {
        var ownProps = Object.keys(obj), i = ownProps.length, resArray = new Array(i); // preallocate the Array
        while (i--)
            resArray[i] = [ownProps[i], obj[ownProps[i]]];
        return resArray;
    };
const Service = serviceModule.Service;
exports.Service = Service;
const Factory = CRDTsModule.factoryAPI;
exports.Factory = Factory;
const CRDTs = { GMap: CRDTsModule.GMap, ORSet: CRDTsModule.ORSet, LWWSet: CRDTsModule.LWWSet };
const Extras = { Conditions: CRDTsModule.Conditions, VectorClock: vectorClockModule.VectorClock };
const Datastructures = { CRDTs: CRDTs, Extra: Extras };
exports.Datastructures = Datastructures;
const Decorators = { accessor: decoratorModule.accessor };
exports.Decorators = Decorators;
const Config = { setBroadcastMechanism: serviceModule.setBroadcast, util: util };
exports.Config = Config;
//# sourceMappingURL=framework.js.map