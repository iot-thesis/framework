Object.defineProperty(exports, "__esModule", { value: true });
const spiders = require('spiders.js');
var serviceModule, serviceDiscoveryModule, RPCPort, application, ipAddress;
function setDependencies(serviceMod, serviceDiscoveryMod, ip) {
    serviceModule = serviceMod;
    serviceDiscoveryModule = serviceDiscoveryMod;
    ipAddress = ip;
}
exports.setDependencies = setDependencies;
class Application extends spiders.Application {
    constructor(ip, port) {
        super(new spiders.SpiderActorMirror(), ip, port);
    }
    // For accessing a strongly consistent object through RPC
    getServiceField(serviceID, name) {
        var service = serviceModule.getService(serviceID);
        return service._REPLICAS_[name].getBaseObject(); // return the base object to RPC on
    }
    // Subscriber registers himself
    registerSubscriber(typeTag, ip) {
        // Returns an array of all the services (stringified) we already published
        return serviceDiscoveryModule.newSubscriber(typeTag, ip);
    }
    // Publisher sent us a service
    receiveService(service) {
        serviceDiscoveryModule.onService(service);
        return true;
    }
    // Someone sent us an update (i.e. mutation of a CvRDT)
    receiveUpdate(updateID, service) {
        serviceDiscoveryModule.onUpdate(updateID, service);
        return true;
    }
    // Someone sent us an operation (i.e. mutation of a CmRDT)
    receiveOperation(operationInfo) {
        operationInfo = JSON.parse(operationInfo);
        serviceDiscoveryModule.onOperation(operationInfo);
        return true;
    }
}
function configure(port) {
    RPCPort = port;
    application = new Application(ipAddress, RPCPort);
}
exports.configure = configure;
function getApplication() {
    return application;
}
exports.getApplication = getApplication;
//# sourceMappingURL=application.js.map