/*
 * Creates a generic factory for a given type.
 */
Object.defineProperty(exports, "__esModule", { value: true });
class Factory {
    constructor(checkMethodName, requiredMethods, requiredStaticMethods) {
        this.Classes = {};
        this.requiredMethods = requiredMethods;
        this.requiredStaticMethods = requiredStaticMethods;
        this[checkMethodName] = function (className) {
            return this.Classes[className] ? true : false;
        };
    }
    factory(type) {
        var clazz = this.Classes[type];
        if (!clazz) {
            throw new TypeError(`Factory has no class named ${type}.\nMake sure to register that class at the factory.`);
        }
        return clazz;
    }
    register(clazz) {
        if (!clazz) {
            throw new TypeError(`Factory expects a class, but got '${clazz}'`);
        }
        if (clazz.name === "") {
            throw new TypeError(`Cannot register anonymous classes.`);
        }
        this.requiredMethods.forEach(method => {
            if (typeof clazz.prototype[method] !== 'function') {
                throw new TypeError(`Could not register ${clazz.name}: does not implement the '${method}' method from the required interface.`);
            }
        });
        this.requiredStaticMethods.forEach(method => {
            // Check that the class implements this static method
            if (typeof clazz[method] !== 'function') {
                throw new TypeError(`Could not register ${clazz.name}: does not implement the static '${method}' method from the required interface.`);
            }
        });
        // All tests passed, hence, the class implements all required methods
        this.Classes[clazz.name] = clazz;
    }
}
exports.default = Factory;
//# sourceMappingURL=factory.js.map