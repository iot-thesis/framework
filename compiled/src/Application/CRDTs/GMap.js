/*
 * Implementation of a grow-only Map.
 * One can only add key-value pairs to this map.
 * Deletion of key-value pairs is not allowed.
 * Updating a key's value is not allowed either.
 *
 * Important: keys must be primitives because
 *            they are compared using the `===` operator,
 *            which does not work for replicated objects since `obj !== replica(obj)`
 */
Object.defineProperty(exports, "__esModule", { value: true });
var exchangeableFactory, util;
function setDependencies(exchangeableFactoryModule, utilities) {
    util = utilities;
    exchangeableFactory = exchangeableFactoryModule;
}
exports.setDependencies = setDependencies;
;
class GMap extends Map {
    // K and V must be primitives in order to be stringify'able and parsable
    constructor(it) {
        if (it) {
            for (var [k, v] of it) {
                util.checkEqable(v, 'GMap values must implement the Eq interface.');
            }
        }
        super(it);
    }
    // Provide own `set` method that has an etxra security check
    set(key, val) {
        util.checkEqable(val, 'GMap values must implement the Eq interface.');
        /*
        if (util.isMutable(val))
            throw new TypeError(`Cannot set the value of '${key}' to a mutable object.`);*/
        if (this.has(key))
            throw new Error(`Cannot update the value of a key (${key}) in a GMap.`);
        else {
            super.set(key, val);
        }
    }
    // Prohibit deletion
    delete(key) {
        throw new Error(`Cannot delete a key-value pair from a GMap.`);
    }
    // Destructive merge
    merge(replica) {
        replica.forEach((value, key) => {
            if (!this.has(key)) {
                this.set(key, value);
            }
        });
    }
    static fromjson(json) {
        return new GMap(json.map(([k, v]) => [k, util.deepFreeze(v)])); // make values immutable
    }
}
exports.GMap = GMap;
//# sourceMappingURL=GMap.js.map