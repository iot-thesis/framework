Object.defineProperty(exports, "__esModule", { value: true });
var VectorClock, serialize, deserialize;
function setDependencies(vectorClock, serializationModule) {
    VectorClock = vectorClock;
    serialize = serializationModule.serialize;
    deserialize = serializationModule.deserialize;
}
exports.setDependencies = setDependencies;
/*
 * Implementation of an LWW-Element-Set (LWW = Last-Write-Wins).
 * API:
 *  - add(element)    --> boolean
 *  - remove(element) --> boolean
 *  - has(element)    --> boolean
 */
class LWWSet {
    constructor(addSets, removeSets) {
        this._addSets = addSets ? addSets : new Map();
        this._removeSets = removeSets ? removeSets : new Map();
    }
    /*
     * elem ∈ LWWSet <==> ∃c ∈ addTags: [∀c' ∈ removeTags: c' < c] where c and c' are timestamps (vector clocks)
     * This version is slightly different, it receives `c` as an argument and checks whether it happened after all removals.
     */
    has(elem, timestamp) {
        var { addSet, removeSet } = this.getSets(elem);
        if (!this._addSets.has(elem)) {
            return false;
        }
        else if (timestamp) {
            if ([...removeSet].find(vector => !VectorClock.happenedBefore(vector, timestamp))) {
                // ∃c' ∈ removeTags: c' ≮ c (where c is `timestamp` argument)
                return false;
            }
            else {
                return true;
            }
        }
        else {
            /* Regular `has`.
               An element is in the LWW Set if it is in the remove set
               but with an earlier timestamp than the latest timestamp in the add set. */
            var { addSet, removeSet } = this.getSets(elem);
            if ([...addSet].find(vc => this.happenedAfterAll(vc, removeSet)))
                return true;
            else
                return false;
        }
    }
    // Checks if a timestamp happened after all timestamps of a set
    happenedAfterAll(timestamp, set) {
        for (var vc of set) {
            if (!VectorClock.happenedBefore(vc, timestamp)) {
                return false;
            }
        }
        return true;
    }
    add(elem, timestamp) {
        // store a copy of the vector clock
        timestamp = deserialize(serialize(timestamp));
        this.addToSet(timestamp, elem, this._addSets);
    }
    remove(elem, timestamp) {
        // store a copy of the vector clock
        timestamp = deserialize(serialize(timestamp));
        this.addToSet(timestamp, elem, this._removeSets);
    }
    getSets(elem) {
        var addSet = this._addSets.has(elem) ? this._addSets.get(elem) : new Set();
        var removeSet = this._removeSets.has(elem) ? this._removeSets.get(elem) : new Set();
        return { addSet: addSet, removeSet: removeSet };
    }
    addToSet(timestamp, elem, sets) {
        if (!sets.has(elem)) {
            sets.set(elem, new Set());
        }
        sets.get(elem).add(timestamp);
    }
    // Returns a copy of this LWW Set
    /*
    copy(): LWWSet<T> {
        function copyMapOfSets(mapSets) {
            var arrayCopy = Array.from(mapSets).map(([k, v]) => [k, Array.from(v)]); // transform map and sets to arrays
            return new Map(arrayCopy.map(([k, v]) => [k, new Set(v)])); // Copy of the map and sets
        }
        
        var setCopy         = new LWWSet();
        setCopy._addSets    = copyMapOfSets(this._addSets);
        setCopy._removeSets = copyMapOfSets(this._removeSets);
        
        return setCopy;
    }
    */
    /*
     * Implement the `tojson`, `fromjson` methods from the CmRDT_I interface.
     */
    tojson() {
        return [this._addSets, this._removeSets];
    }
    static fromjson(json) {
        const [addSets, removeSets] = json;
        return new LWWSet(addSets, removeSets);
    }
}
exports.LWWSet = LWWSet;
//# sourceMappingURL=LWW-Set.js.map