Object.defineProperty(exports, "__esModule", { value: true });
///<reference path='../../../../node_modules/immutable/dist/immutable.d.ts'/>
const conditions_1 = require("./conditions");
const Immutable = require("immutable");
const replica_1 = require("../../replica");
var Operation, LWWSet, ExchangeableFactory, serialize, deserialize, ipAddress, VectorClock, util;
function setDependencies(LWW, efactoryModule, serializationModule, ip, vectorClock, utilities, OperationModule) {
    Operation = OperationModule;
    LWWSet = LWW;
    ipAddress = ip;
    util = utilities;
    serialize = serializationModule.serialize;
    deserialize = serializationModule.deserialize;
    VectorClock = vectorClock;
    ExchangeableFactory = efactoryModule;
    ExchangeableFactory.register(Operation);
    ExchangeableFactory.register(SECRO);
    ExchangeableFactory.register(conditions_1.default);
    ExchangeableFactory.register(Version);
}
exports.setDependencies = setDependencies;
/*
 * CmRDT version is a tuple:
 * (version number, commit operation)
 */
class Version {
    // _commit is the commit operation that led to this version
    constructor(_version, _commit) {
        this._version = _version;
        this._commit = _commit;
    }
    getVersion() {
        return this._version;
    }
    getLatestCommit() {
        return this._commit;
    }
    setLatestCommit(commit) {
        this._commit = commit;
    }
    tojson() {
        return { version: this._version, commit: this._commit };
    }
    static fromjson(json) {
        const { version, commit } = json;
        return new Version(version, commit);
    }
}
/*
 * Operation-based implementation of a JSON CRDT datatype.
 */
var jsonCtr = 0;
class SECRO extends replica_1.Replica {
    constructor(name, object, accessors, preconditions, postconditions, id, version) {
        if (!(preconditions instanceof conditions_1.default && postconditions instanceof conditions_1.default)) {
            throw new TypeError('Pre- and post-conditions must be of type \'Conditions\'.');
        }
        super(name, object);
        this._id = id ? id : `${ipAddress}:${jsonCtr++}`;
        this._preconditions = preconditions;
        this._postconditions = postconditions;
        this._clock = new VectorClock();
        this._operationHistory = Immutable.List();
        this._accessors = accessors;
        // The dummy commit is an operation that "commits" the initial base object (i.e. version -1 to version 0).
        const dummyCommit = new Operation('commit', [this.getBaseObject()], new VectorClock(), this._id, -1, true);
        this._version = version ? version : new Version(0, dummyCommit);
    }
    // Returns the current version number
    getVersion() {
        return this._version.getVersion();
    }
    // Returns a boolean indicating if that name corresponds to an accessor
    isAccessor(name) {
        return this._accessors.includes(name);
    }
    // "Executes" `op(args)` on the base object
    execute(op, args) {
        var operation = new Operation(op, args, this._clock, this._id, this.getVersion());
        return { operation: operation, result: this.receive(operation, true).result };
    }
    // Receives an update and sends it to the target field
    receive(operation, noUpdate) {
        // Ensure we did not already process the operation
        if (this.getHistory().find(op => op.id === operation.id))
            return { resultObject: this.getBaseObject(), result: null };
        // Treat commit operations specially
        if (operation.name === "commit") {
            // Update the clock upon commit operations coming from other peers!
            if (!noUpdate)
                this._clock.update(operation.clock);
            return this.commit(operation);
        }
        /*
         * Drop the received operation in the following cases:
         *  - the operation applies to an older version
         *  - the operation is concurrent with the previous commit
         */
        if (operation.version < this.getVersion() || VectorClock.isConcurrent(operation.clock, this._version.getLatestCommit().clock)) {
            return { resultObject: this.getBaseObject(), result: null };
        }
        var op = this.getBaseObject()[operation.name];
        if (operation.objectID !== this._id) {
            throw new Error(`Update not applicable.`);
        }
        if (operation.name !== '__addProperty__' && operation.name !== '__deleteProperty__' && (!op || typeof op !== 'function')) {
            throw new TypeError(`${operation.name} is not a function.`);
        }
        /*
         * Update our clock, if we received this operation from another peer.
         * If it comes from our own peer (i.e. from `execute` above), then we may not update our clock again since
         * `execute` already updated it (by creating a new operation).
         */
        if (!noUpdate) {
            this._clock.update(operation.clock);
        }
        // Add operation to the history at the RIGHT place!
        var res = this.insert(operation);
        this.notifyObserver(res.resultObject); // call observer since the operation history changed
        if (!noUpdate) {
            this.notifyRemoteObserver(res.resultObject);
        }
        return res;
    }
    /*
     * Checks that the received `commit` operation commits the current version.
     * If the commit operation commits the same version as our latest commit, then both commits must be concurrent.
     * To make concurrent commits (of the same version) commutative we always chose the commit with the smallest ID.
     */
    commit(commit) {
        const version = commit.version, state = commit.args[0];
        /*
         * The returned result incorporates a boolean `result`,
         * indicating whether the commit was successfull or not.
         */
        if (version === this._version.getVersion()) {
            // The received operation commits our current version
            this._operationHistory = Immutable.List();
            this.setBaseObject(state);
            this._version = new Version(version + 1, commit);
            return { resultObject: this.getBaseObject(), result: true };
        }
        else if (version === this._version.getVersion() - 1) {
            // The received operation commits the previous version
            // and should thus be concurrent with the commit we already performed and which resulted in the current version.
            if (!VectorClock.isConcurrent(commit.clock, this._version.getLatestCommit().clock)) {
                throw new Error(`Two commits cannot commit the same state without being concurrent!`); // sanity check
            }
            // Retain the commit with the smallest ID
            if (commit.id.localeCompare(this._version.getLatestCommit().id) === -1) {
                // The received commit is concurrent with our latest commit, both commit the same version
                // AND the ID of the received commit is smaller than our latest commit.
                // Therefore, we retain this newly received commit operation.
                // Notice that we DO NOT UPDATE THE VERSION NUMBER, since the latest commit already did that.
                this._operationHistory = Immutable.List();
                this.setBaseObject(state);
                this._version.setLatestCommit(commit);
                return { resultObject: this.getBaseObject(), result: true };
            }
        }
        else {
            return { resultObject: this.getBaseObject(), result: false };
        }
    }
    /*
     * Returns an array [ccOps, ccGroups, groupIndexes]
     * where `ccOps` is an immutable list containing all operations from `history` that are concurrent with the received `operation`,
     * `ccGroups` is an immutable list containing all groups from `history` that are concurrent with the received `operation`,
     * and `groupIndexes` is an immutable list containing the indexes of the corresponding `ccGroups` in `history`.
     */
    getConcurrentOpsAndGroups(history, operation) {
        var myId = operation.id;
        var ccOps = Immutable.List();
        var ccGroups = Immutable.Set();
        var groupIdxs = Immutable.List();
        history.forEach((group, gIdx) => group.forEach(op => {
            if (myId !== op.id && VectorClock.isConcurrent(op.clock, operation.clock)) {
                ccOps = ccOps.push(op.id);
                ccGroups = ccGroups.add(group);
                groupIdxs = groupIdxs.push(gIdx);
            }
        }));
        return [ccOps, ccGroups.toList(), groupIdxs];
    }
    /*
     * Compares two operations, A and B.
     * Operation A < B if A happened before B or A is concurrent with B and replicaID of A < replicaID of B.
     * This can be used to sort an group of operations according to the following total order:
     *   (c1, p1) < (c2, p2) iff (c1 < c2) ∨ (c1|c2 ∧ p1 < p2)
     *   where c<i> = clock of the i-th operation
     *         p<i> = replica ID of the i-th operation
     */
    compareOperations(o1, o2) {
        var clock1 = o1.clock;
        var clock2 = o2.clock;
        if (VectorClock.happenedBefore(clock1, clock2))
            return -1;
        else if (VectorClock.happenedBefore(clock2, clock1))
            return 1;
        else
            // They are concurrent, impose some ordering
            return o1.id.localeCompare(o2.id);
    }
    // Deletes the given operation from the operation history.
    removeOperation(op) {
        this._operationHistory = this._operationHistory.map((group) => group.filter((o) => o.id !== op.id));
    }
    // Adds the operation to the operation history.
    insert(operation) {
        var history = this._operationHistory;
        // Compute which operations are concurrent with this operation
        var [ccOps, ccGroups, groupIndexes] = this.getConcurrentOpsAndGroups(history, operation);
        if (ccOps.size === 0) {
            // Binary search should not find a group since we are concurrent with no operations
            var lookup = util.binarySearch(operation, history.toJS(), operationLesserThen);
            if (lookup.found)
                throw new Error('Something is wrong with the binary search algorithm.');
            history = history.insert(lookup.index, Immutable.List().push(operation)); // insert the operation as a new group
            var { isValid, resultObject, result } = this.replayOperations(history, operation);
            if (!isValid)
                throw new Error(`Cannot execute '${operation.name}(${operation.args})': no valid execution exists.`);
            this._operationHistory = history;
            return { resultObject: resultObject, result: result };
        }
        else {
            /* Operation is concurrent with at least one other operation.
               Hence, the operation is also concurrent with at least one group.
               Merge those groups into one, and add the operation to it. */
            var firstGroupIdx = groupIndexes.first();
            var mergedGroup = ccGroups.reduce((merged, group) => merged.concat(group), Immutable.List());
            /*
             * Add this operation to the group and sort the resulting group of operations.
             * Sorting the group ensures determinism of this `insert` method,
             * since permutations will be generated in the same order.
             */
            var newGroup = mergedGroup.push(operation)
                .sort(this.compareOperations);
            // Remove the groups that were merged
            groupIndexes.reverse()
                .forEach(idx => history = history.delete(idx));
            for (var permutation of util.permute(newGroup.toArray())) {
                // Ensure the permutation does not violate causality
                if (obeysCausality(permutation)) {
                    // Construct the potential history
                    var pHistory = history.insert(firstGroupIdx, Immutable.fromJS(permutation));
                    var { isValid, resultObject, result } = this.replayOperations(pHistory, operation);
                    if (isValid) {
                        this._operationHistory = pHistory;
                        return { resultObject: resultObject, result: result };
                    }
                }
            }
            // If we reached here, none of the permutations are valid
            throw new Error(`Cannot execute '${operation.name}(${operation.args})': no valid execution exists.`);
        }
    }
    // Returns a copy of the operations history
    copyHistory(history) {
        return history.map(group => group.map(op => op.copy()));
    }
    /*
     * Takes an operation history and the operation of interest.
     * If the history is valid, it replays the complete history
     * and returns the resulting base object and the return value of the operation of interest.
     */
    replayOperations(history, addedOperation) {
        /*
         * We work on copies of the operations that are nested within the history because executing the user's operations
         * might have side-effects on arguments passed by the user. We don't want these side-effects
         * to alter the operations from the history, which could result in subtle errors.
         */
        history = this.copyHistory(history);
        var propertySet = new LWWSet(); // property set of the base object copy below
        var baseObjectCopy = util.copyObject(this.getBaseObject());
        var baseObjects = new Map(); // maps operations to the state of the baseObject on which they were executed
        var results = new Map(); // maps operations to their result
        var res = null;
        for (var group of history[Symbol.iterator]()) {
            // For each operation in this group: check its preconditions and execute it
            for (var operation of group[Symbol.iterator]()) {
                var timestamp = operation.clock;
                if (operation.name === '__addProperty__') {
                    //var [prop, value] = operation.args;
                    var prop = operation.args[0];
                    var value = operation.args[1];
                    propertySet.add(prop, timestamp);
                    if (propertySet.has(prop, timestamp)) {
                        baseObjectCopy[prop] = value;
                    }
                }
                else if (operation.name === '__deleteProperty__') {
                    var prop = operation.args[0];
                    propertySet.remove(prop, timestamp);
                    if (!propertySet.has(prop, timestamp)) {
                        delete baseObjectCopy[prop];
                    }
                }
                else {
                    // Check precondition
                    if (this._preconditions.has(operation.name) &&
                        !this._preconditions.get(operation.name).apply(baseObjectCopy, [baseObjectCopy, ...operation.args])) {
                        // violated
                        return { isValid: false, resultObject: null, result: null };
                    }
                    // Execute operation
                    baseObjects.set(operation.id, util.copyObject(baseObjectCopy)); // store the state on which the operation is executed
                    var retVal = operation.execute(baseObjectCopy);
                    results.set(operation.id, retVal); // store the operation's result
                    if (addedOperation && operation.equals(addedOperation)) {
                        res = retVal;
                    }
                }
            }
            // Check postconditions now that all concurrent operations executed
            for (var operation of group[Symbol.iterator]()) {
                const originalState = baseObjects.get(operation.id), result = results.get(operation.id);
                if (this._postconditions.has(operation.name) &&
                    !this._postconditions.get(operation.name).apply(baseObjectCopy, [baseObjectCopy, originalState, operation.args, result])) {
                    // violated
                    return { isValid: false, resultObject: null, result: null };
                }
            }
        }
        return { isValid: true, resultObject: baseObjectCopy, result: res };
    }
    // Adds a property to the base object, the value must be a primitive or a frozen exchangeable object!
    add(property, value) {
        //if (util.isMutable(value))
        //    throw new TypeError('Property value must be immutable.');
        var op = new Operation('__addProperty__', [property, value], this._clock, this._id, this.getVersion());
        return { operation: op, resultObject: this.receive(op, true).resultObject };
    }
    // Removes a property of the base object
    delete(property) {
        var op = new Operation('__deleteProperty__', [property], this._clock, this._id, this.getVersion());
        return { operation: op, resultObject: this.receive(op, true).resultObject };
    }
    // Merges the given operation history with our history
    mergeHistory(history) {
        const ourHistory = this.getHistory().map(op => op.id);
        const newOps = history.filter(op => !ourHistory.contains(op.id)); // operations we did not process yet
        newOps.forEach(op => this.receive(op));
    }
    getObject() {
        return this.replayOperations(this._operationHistory).resultObject;
    }
    // Flattens the operation history
    getHistory(history) {
        var h = history ? history : this._operationHistory;
        // return h.reduce((hist, slot) => [...hist, ...slot], []);
        return h.flatten(); //.toJS();
    }
    equals(replica) {
        // Two replicas are equal if they received the same operations (the order does not matter)
        //var thisOps = this.getHistory().reduce((set, op) => set.add(op.id), new Set());
        //var repOps  = replica.getHistory().reduce((set, op) => set.add(op.id), new Set());
        var thisOps = Immutable.Set(this.getHistory());
        var repOps = Immutable.Set(replica.getHistory());
        return thisOps.equals(repOps);
    }
    getRawHistory() {
        return this._operationHistory;
    }
    tojson() {
        return { id: this._id, name: this.name, baseObject: this.getBaseObject(), version: this._version, clock: this._clock, history: this._operationHistory,
            preconditions: this._preconditions, postconditions: this._postconditions, accessors: this._accessors };
    }
    static fromjson(json) {
        var { id, name, baseObject, version, clock, history, preconditions, postconditions, accessors } = json;
        var crdt = new SECRO(name, baseObject, accessors, preconditions, postconditions, id, version);
        crdt._clock = clock;
        crdt._operationHistory = Immutable.fromJS(history);
        return crdt;
    }
    //// Below methods are required for replicas
    replicate() {
        return { type: 'CmRDT', crdt: serialize(this) };
    }
    static fromReplica(owningService, name, crdt) {
        var replica = owningService.makeCmRDT(name, crdt.getBaseObject(), crdt);
        return replica;
    }
}
exports.SECRO = SECRO;
function operationLesserThen(op1, op2) {
    /*
     * An operation happened before a group of concurrent operations
     * iff it happened before all of them!
     *
     * A group of concurrent operations happened before another operation
     * iff all operations from the group happened before the other operation.
     *
     * e.g.: Vector clock <2, 0, 2> and concurrent clocks <0, 1, 2> and <3, 0, 2>.
     *       <2, 0, 2> is concurrent with <0, 1, 2>
     *       <2, 0, 2> happened before <3, 0, 2>
     *       Hence the ordering should be: <2, 0, 2> <3, 0, 2> <0, 1, 2> or <2, 0, 2> <0, 1, 2> <3, 0, 2>
     */
    if (op2.constructor === Array) {
        // op1 must have happened before at least one of the concurrent operations in op2
        //return op2.find(op => VectorClock.happenedBefore(op1.clock, op.clock));
        for (var i = 0; i < op2.length; i++) {
            var op = op2[i];
            if (!VectorClock.happenedBefore(op1.clock, op.clock)) {
                return false;
            }
        }
        return true;
    }
    else {
        // op1 is a collection of concurrent operations
        //return op1.find(op => VectorClock.happenedBefore(op.clock, op2.clock));
        for (var i = 0; i < op1.length; i++) {
            var op = op1[i];
            if (!VectorClock.happenedBefore(op.clock, op2.clock)) {
                return false;
            }
        }
        return true;
    }
}
/*
 * Checks that a given order of operations is valid.
 * For each pair op operations, x and y, it checks that x appears before y if x happened before y.
 */
function obeysCausality(operationsOrder) {
    return !operationsOrder.find((op, idx) => {
        for (var i = idx + 1; i < operationsOrder.length; i++) {
            if (VectorClock.happenedBefore(operationsOrder[i].clock, op.clock)) {
                return true;
            }
        }
        return false;
    });
}
////// DEBUG //////
const now = (unit) => {
    const hrTime = process.hrtime();
    switch (unit) {
        case 'milli': return hrTime[0] * 1000 + hrTime[1] / 1000000;
        case 'micro': return hrTime[0] * 1000000 + hrTime[1] / 1000;
        case 'nano': return hrTime[0] * 1000000000 + hrTime[1];
        default: return hrTime[0] * 1000000000 + hrTime[1];
    }
};
//////////////////
//# sourceMappingURL=SECRO.js.map