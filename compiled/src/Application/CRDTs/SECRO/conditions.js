/*
 * Representation of pre- and post-conditions.
 */
Object.defineProperty(exports, "__esModule", { value: true });
class Conditions {
    constructor() {
        this._conditions = new Map();
    }
    has(e) {
        return this._conditions.has(e);
    }
    get(e) {
        return this._conditions.get(e);
    }
    // Adds a `validator` condition on `operation`.
    add(operation, validator) {
        this._conditions.set(operation, validator);
    }
    tojson() {
        return this._conditions;
    }
    static fromjson(json) {
        var conditions = new Conditions();
        conditions._conditions = json;
        return conditions;
    }
}
exports.default = Conditions;
//# sourceMappingURL=conditions.js.map