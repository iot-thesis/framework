/*
 * This module exports the implemented CRDTs that are available for the users of our framework.
 * This module also creates the necessary factories such that we can reconstruct a received replica,
 * without requiring that class to be in the global scope (we can simply ask a reference to a given class at the factory).
 */
Object.defineProperty(exports, "__esModule", { value: true });
const factory_1 = require("../factory");
const GMapModule = require("./GMap");
const ORSetModule = require("./ORSet");
const LWWSetModule = require("./LWW-Set");
const conditions_1 = require("./SECRO/conditions");
exports.Conditions = conditions_1.default;
const SECROModule = require("./SECRO/SECRO");
const GMap = GMapModule.GMap;
exports.GMap = GMap;
const SECRO = SECROModule.SECRO;
exports.SECRO = SECRO;
const ORSet = ORSetModule.ORSet;
exports.ORSet = ORSet;
const LWWSet = LWWSetModule.LWWSet;
exports.LWWSet = LWWSet;
// Create an Exchangeable factory
const ExchangeableInterfaceMethods = ['tojson'];
const ExchangeableStaticMethods = ['fromjson'];
const ExchangeableFactory = new factory_1.default('isExchangeable', ExchangeableInterfaceMethods, ExchangeableStaticMethods);
exports.ExchangeableFactory = ExchangeableFactory;
// Create CRDT factories and register the CRDTs
const CvRDTInterfaceMethods = ['merge', ...ExchangeableInterfaceMethods];
const CvRDTStaticMethods = ExchangeableStaticMethods;
const CvRDTFactory = new factory_1.default('isCvRDT', CvRDTInterfaceMethods, CvRDTStaticMethods);
exports.CvRDTFactory = CvRDTFactory;
const CmRDTInterfaceMethods = ExchangeableInterfaceMethods;
const CmRDTStaticMethods = ExchangeableStaticMethods;
const CmRDTFactory = new factory_1.default('isCmRDT', CmRDTInterfaceMethods, CmRDTStaticMethods);
exports.CmRDTFactory = CmRDTFactory;
// API for the end-user of our framework
const factoryAPI = { registerExchangeableClass: ExchangeableFactory.register.bind(ExchangeableFactory),
    registerCvRDTClass: function (clazz) {
        // A CRDT is always serializable, hence also register it at the serializable factory
        ExchangeableFactory.register.call(ExchangeableFactory, clazz);
        CvRDTFactory.register.call(CvRDTFactory, clazz);
    },
    registerCmRDTClass: function (clazz) {
        // A CRDT is always serializable, hence also register it at the serializable factory
        ExchangeableFactory.register.call(ExchangeableFactory, clazz);
        CmRDTFactory.register.call(CmRDTFactory, clazz);
    }
};
exports.factoryAPI = factoryAPI;
factoryAPI.registerCvRDTClass(GMap);
factoryAPI.registerCvRDTClass(ORSet);
factoryAPI.registerCmRDTClass(LWWSet);
// Pass factories to modules that depend on it
function setDependencies(operationModule, serializationModule, ipAddress, VectorClock, util) {
    GMapModule.setDependencies(ExchangeableFactory, util);
    SECROModule.setDependencies(LWWSet, ExchangeableFactory, serializationModule, ipAddress, VectorClock, util, operationModule);
    ORSetModule.setDependencies(ipAddress, util);
    LWWSetModule.setDependencies(VectorClock, serializationModule);
}
exports.setDependencies = setDependencies;
//# sourceMappingURL=CRDT-datastructures.js.map