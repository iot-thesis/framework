Object.defineProperty(exports, "__esModule", { value: true });
const dgram = require('dgram');
const spiders = require('spiders.js');
const socket = dgram.createSocket('udp4');
var multicastPort, multicastAddress, applicationModule, RPCPort;
var farRefs = new Map(); // ip -> farRef
function setDependencies(applicationMod) {
    applicationModule = applicationMod;
}
exports.setDependencies = setDependencies;
function configure(mcAddress, mcPort, rpcPort) {
    multicastAddress = mcAddress;
    multicastPort = mcPort;
    RPCPort = rpcPort;
    /*
     * Received messages are broadcasted again, in order to achieve transitive communication.
     * An example: Assume 3 nodes: A, B and C.
     *             Assume A only has wifi, B has wifi and bluetooth and C only has bluetooth.
     *             Assume the following direct communication links (->) : A --> B, B --> C but not A --> C
     *             Because B will broadcast received messages over wifi as well as bluetooth,
     *             this will result in C receiving A's message, although A and C can never directly communicate witch each other.
     *             Hence, we achieved transitivity.
     */
    socket.bind(multicastPort, () => {
        socket.setBroadcast(true);
        socket.setMulticastTTL(255);
        socket.addMembership(multicastAddress); // Join the multicast group given by the address 
    });
}
exports.configure = configure;
function multicast(buffer) {
    socket.send(buffer, 0, buffer.length, multicastPort, multicastAddress);
}
exports.broadcast = multicast;
function onMessage(callback) {
    socket.on('listening', () => {
        socket.on('message', callback);
    });
}
exports.onMessage = onMessage;
/*
 * When we discover a peer, `makeFarRef` will be called
 * to establish a direct communication link with the peer.
 */
function makeFarRef(ip) {
    if (farRefs.has(ip)) {
        return farRefs.get(ip);
    }
    var farRefPromise = applicationModule.getApplication().libs.remote(ip, RPCPort); // a promise
    farRefs.set(ip, farRefPromise);
    return farRefPromise;
}
exports.makeFarRef = makeFarRef;
function hasFarRef(ip) {
    return farRefs.has(ip);
}
exports.hasFarRef = hasFarRef;
function getAllFarRefs() {
    return Array.from(farRefs.values());
}
exports.getAllFarRefs = getAllFarRefs;
//# sourceMappingURL=plugin.js.map