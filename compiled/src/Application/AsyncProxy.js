Object.defineProperty(exports, "__esModule", { value: true });
var Replica, util;
function setDependencies(replicaClass, utilities) {
    Replica = replicaClass;
    util = utilities;
}
exports.setDependencies = setDependencies;
const handler = {
    get: function (target, prop, receiver) {
        // Field accesses must return promises
        if (typeof target[prop] === "object") {
            return new Promise(resolve => resolve(target._CS_Promise_Proxies_.get(prop))); //return new Promise(resolve => resolve(target[prop]));
        }
        else if (typeof target[prop] === "function") {
            return target._CS_Promise_Proxies_.get(prop).bind(target);
        }
        else {
            return target[prop];
        }
    }
};
// Makes a function's result asynchronous
function makeFunResAsync(fun) {
    return new Proxy(fun, {
        apply: function (target, thisArg, args) {
            // Result of a function call must be wrapped in a promise
            // if the result is an object we must also deeply promisify it.
            var res = target.call(thisArg, ...args);
            if (typeof res === "object" && !(res instanceof Replica))
                res = AsyncProxy(res);
            return new Promise(resolve => resolve(res));
        }
    });
}
/*
 * Makes an object deeply asynchronous as if it is remote.
 * Field accesses return a promise as well as method invocations.
 */
function AsyncProxy(obj) {
    const fields = new Set();
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            fields.add(prop);
    }
    fields.add(...util.getAllMethods(obj)); // because ES6 class methods are not enumerable...
    obj._CS_Promise_Proxies_ = new Map();
    // Iterate over all fields of `obj`.
    // For each field that is an object make that object asynchronous (if it is not a strongly consistent proxy).
    fields.forEach(prop => {
        if (prop !== "_CS_Promise_Proxies_") {
            if (typeof obj[prop] === "object" && !(obj[prop] instanceof Replica))
                obj._CS_Promise_Proxies_.set(prop, AsyncProxy(obj[prop]));
            else if (typeof obj[prop] === "function") {
                obj._CS_Promise_Proxies_.set(prop, makeFunResAsync(obj[prop]));
            }
        }
    });
    return new Proxy(obj, handler);
}
exports.AsyncProxy = AsyncProxy;
//# sourceMappingURL=AsyncProxy.js.map