'lang sweet.js';

/*
 * Helper file for the transpiler.
 * Stores state accross multiple macros.
 */

const typetags = new Set();
export function registerTypetag(tag) {
    typetags.add(tag);
}

export function hasTypetag(tag) {
    return typetags.has(tag);
}

export function getTypetags() {
    return Array.from(typetags);
}