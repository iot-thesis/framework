const ip = require('ip');
const ipAddress = ip.address();

if (ipAddress === '127.0.0.1')
    throw new Error(`No network connection (${ipAddress}). There should be a network connection at startup.`);

/*
 * We avoid importing modules multiple times.
 * Therefore we import them all only once, here.
 * Modules that are dependent on other modules will be initialized with their dependencies.
 */

import * as jsExtensions              from './extensions';
import * as applicationModule         from './Application/application';
import * as replicaModule             from './Application/replica';
import * as asyncProxyModule          from './Application/AsyncProxy';
import * as lanModule                 from './Application/Communication/Plugins/LAN/plugin';
import * as serviceDiscoveryModule    from './Application/Communication/serviceDiscovery';
import * as serviceModule             from './Application/service';
import * as operationModule           from './Application/CRDTs/SECRO/operation';
import * as CRDTsModule               from './Application/CRDTs/CRDT-datastructures';
import * as serializationModule       from './Application/serialization';
import * as vectorClockModule         from './Application/vector-clock';
import * as decoratorModule           from './decorators';
import * as util                      from './Application/util';

// Initialize modules that have dependencies
lanModule.setDependencies(applicationModule);

// Configure the LAN module
const RPCPort = 8000;
const multicastPort = 8001; //8080;
const multicastAddress = '225.0.0.1';
lanModule.configure(multicastAddress, multicastPort, RPCPort);

serializationModule.setDependencies(CRDTsModule.ExchangeableFactory, util);
operationModule.setDependencies(ipAddress, util);
CRDTsModule.setDependencies(operationModule.Operation, serializationModule, ipAddress, vectorClockModule.VectorClock, util);
jsExtensions.setDependencies(CRDTsModule.ExchangeableFactory, util);
applicationModule.setDependencies(serviceModule, serviceDiscoveryModule, ipAddress);

// Configure the application module
applicationModule.configure(RPCPort);

serviceDiscoveryModule.setDependencies(applicationModule, serviceModule, replicaModule, lanModule, serializationModule, ipAddress, CRDTsModule.SECRO);
serviceModule.setDependencies(replicaModule, serviceDiscoveryModule, lanModule, CRDTsModule, serializationModule, CRDTsModule.SECRO, util, ipAddress);
asyncProxyModule.setDependencies(replicaModule.Replica, util);
replicaModule.setDependencies(serviceModule, CRDTsModule, serviceDiscoveryModule, CRDTsModule.CvRDTFactory, CRDTsModule.CmRDTFactory, asyncProxyModule.AsyncProxy, serializationModule, ipAddress, util);
vectorClockModule.setDependencies(CRDTsModule.ExchangeableFactory, ipAddress);
util.setDependencies(CRDTsModule.Conditions, serializationModule, CRDTsModule.CvRDTFactory, CRDTsModule.CmRDTFactory, replicaModule);

// Polyfill for Object.entries which apparently does not exist in nodeJS
if (!(<any>Object).entries)
    (<any>Object).entries = function( obj ){
        var ownProps = Object.keys( obj ),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array
        while (i--)
            resArray[i] = [ownProps[i], obj[ownProps[i]]];
        
        return resArray;
    };

const Service = serviceModule.Service;
const Factory = CRDTsModule.factoryAPI;
const CRDTs = { GMap: CRDTsModule.GMap, ORSet: CRDTsModule.ORSet, LWWSet: CRDTsModule.LWWSet };
const Extras = { Conditions: CRDTsModule.Conditions, VectorClock: vectorClockModule.VectorClock };
const Datastructures = { CRDTs: CRDTs, Extra: Extras };
const Decorators = { accessor: decoratorModule.accessor };
const Config = { setBroadcastMechanism: serviceModule.setBroadcast, util: util };

export { Service, Datastructures, Factory, Decorators, Config, serializationModule, ipAddress };