/*
 * Method decorator for accessors.
 * Adds the method's name to a static property `accessors` of its class.
 * Can be used to decorate methods with the `@accessor` annotation.
 */
export function accessor(target, methodName, descriptor) {
    if (target.constructor.hasOwnProperty('accessors')) {
        target.constructor.accessors.add(methodName);
    }
    else {
        target.constructor.accessors = (new Set()).add(methodName);
    }
}