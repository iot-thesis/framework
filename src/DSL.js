// Only load the framework if it was not yet loaded.
if (!(global.ORSet && global.LWWSet && global.GMap && global.Service && global.Factory && global.ipAddress)) {
    const framework = require('./framework');

    // Set globals
    global.ORSet     = framework.Datastructures.CRDTs.ORSet;
    global.LWWSet    = framework.Datastructures.CRDTs.LWWSet;
    global.GMap      = framework.Datastructures.CRDTs.GMap;
    global.Service   = framework.Service;
    global.Factory   = framework.Factory;
    global.ipAddress = framework.ipAddress;
    global.accessor  = framework.Decorators.accessor;
}