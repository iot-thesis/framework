/*
 * This file extends native JS datastructures with additional functionality.
 * Arrays, maps and sets are made Eq'able by implementing `equals` method.
 * Maps and sets are also made Exchangeable by implementing `tojson` and static `fromjson` methods and registering at the factory.
 */

var util;
export function setDependencies(exchangeableFactory, utilities) {
    util = utilities;
    exchangeableFactory.register(Map);
    exchangeableFactory.register(Set);
}

Array.prototype.equals = function(replica) {
    if (this.length !== replica.length) { return false; }
    
    for (var i = 0; i < this.length; i++) {
        var ourElement = this[i];
        var repElement = replica[i];
        
        if (!util.equals(ourElement, repElement))
            return false;
    }
    
    return true;
};

/* wss wilt ge dit niet he, en als ge dit wilt dan met getOwnPropertyNames, maar wilt ge niet want
   elk object implementeert dan equals
Object.prototype.equals = function(replica) {
    var thisEntries    = Object.entries(this);
    var replicaEntries = Object.entries(replica);
    
    if (thisEntries.length !== replicaEntries.length) { return false; }
    
    // Dit hieronder is zware code duplicatie tov EArray --> zien om te abstraheren in util ofzo
    // (een functie die dit doet en simpelweg een array van [prop, val] binnenkrijgt (prop is bij array dan idx))
    for (const [prop, val] of thisEntries) {
        let replicaVal  = replica[prop];
        
        if (util.implementsEqable(val) && util.implementsEqable(replicaVal)) {
            if (!val.equals(replica[prop])) {
                return false;
            }
        }
        else {
            // Use default `===` operator
            if (val !== replica[prop]) {
                return false;
            }
        }
    }
    
    return true;
}; */

Map.prototype.tojson = function() {
    return Array.from(this);
};

Map.fromjson = function(array) {
    return new Map(array);
};

Map.prototype.equals = function(replica) {
    if (this.size !== replica.size) {
        return false;
    }
    else {
        for (const [k, v1] of this) {
            if (!replica.has(k)) {
                return false;
            }
            else {
                var v2 = replica.get(k);
                if (!util.equals(v1, v2))
                    return false;
                
                /*
                if (util.implementsEqable(v1) && util.implementsEqable(v2)) {
                    if (!v1.equals(v2))
                        return false;
                }
                else if (v1 !== v2) {
                    return false;
                }
                */
            }
        }
    }
    
    return true;
};

Map.prototype.getOrElse = function(key, notSetValue) {
    return this.has(key) ? this.get(key) : notSetValue;
};

Set.prototype.tojson = function() {
    return Array.from(this);
};

Set.fromjson = function(array) {
    return new Set(array);
};

// Checks whether this set is a subset of the provided set.
Set.prototype.subset = function(superset) {
    var subsetA   = Array.from(this);
    var supersetA = Array.from(superset);
    
    for (const e of subsetA) {
        if (util.implementsEqable(e)) {
            if (supersetA.findIndex(elem => e.equals(elem)) === -1)
                return false;
        }
        else if (!superset.has(e)) {
            return false;
        }
    }
    
    return true;
};

Set.prototype.equals = function(replica) {
    return this.subset(replica) && replica.subset(this);
};