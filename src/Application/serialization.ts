/*
 * Implementation of "generic" serialization/deserialization functions,
 * which are able to (de)serialize any serializable object.
 * These objects must be registered at the Serializable factory.
 */

var ExchangeableFactory, util;
export function setDependencies(ExchangeableFactoryModule, utilities) {
    ExchangeableFactory = ExchangeableFactoryModule;
    util = utilities;
}

//export function serialize(val : any): string {
//    if (typeof val === 'object' && val !== null) {
//        var type = val.constructor.name;
//        if (ExchangeableFactory.isExchangeable(type)) {
//            //throw new TypeError(`Cannot serialize objects that are not register at the 'Exchangeable' factory.`);
//            // stringify uses the object's toJSON method which is required by the `Exchangeable` interface
//            return JSON.stringify({ type: type, value: val.toJSON() });
//        }
//        else {
//            // regular object
//            return JSON.stringify(val); //JSON.stringify({ frozen: Object.isFrozen(val), obj: val });
//        }
//    }
//    else if (typeof val === 'function') {
//        return JSON.stringify({ type: 'function', value: val.toString() });
//    }
//    else {
//        // Primitive
//        return JSON.stringify(val);
//    }
//    //return JSON.stringify(obj, replacer);
//}

/*
 * Recursively serializes its input.
 * Exchangeable classes are serialized using their provided `toJSON` method.
 * Functions are serialized too.
 */
export function serialize(v : any): string {
    function replacer(key, val : any) {
        if (typeof val === 'object' && val !== null) {
            var type = val.constructor.name;
            if (ExchangeableFactory.isExchangeable(type)) {
                //var json = val.tojson();
                //if (json === val) { json. }
                return { __CS__type__: 'userDefinedObject', kind: type, value: val.tojson() };
            }
            else {
                // Regular object that does not define a custom serialization protocol
                return val;
            }
        }
        else if (typeof val === 'function') {
            // Serialize function
            return { __CS__type__: 'function', value: val.toString() };
        }
        else {
            // Primitive
            return val;
        }
    }
    
    return JSON.stringify(v, replacer);
}

/*
 * Recursively parses a string.
 * Exchangeable objects are reconstructed using their `fromjson` method.
 * Functions are also reconstructed and must therefore only be used within isolates.
 */
export function deserialize(str : string): any {
    function reviver (key, json) {
        if (typeof json === 'object' && json !== null) {
            // It is a function or a user defined or regular object
            if (!json.hasOwnProperty('__CS__type__')) {
                return json;
            }
            else {
                if (json.__CS__type__ === 'function') {
                    return eval(constructMethod(json.value));
                }
                else if (json.__CS__type__ === 'userDefinedObject') {
                    var Clazz = ExchangeableFactory.factory(json.kind);
                    return Clazz.fromjson(json.value);
                }
                else {
                    throw new Error('Unrecognized object while parsing.');
                }
            }
        }
        else {
            // Primitive
            return json;
        }   
    }
    
    return JSON.parse(str, reviver);
}

//export function deserialize(str : string): any {
//    var json = JSON.parse(str);
//    if (typeof json === 'object' && json !== null && json.type) {
//        if (json.type === 'function') {
//            return eval(constructMethod(json.value));
//        }
//        else {
//            // serializable object
//            var Clazz = ExchangeableFactory.factory(json.type);
//            return Clazz.fromJSON(json.value);
//        }
//    }
//    /*
//    else if (typeof json === 'object' && json !== null && json.hasOwnProperty('frozen')) {
//        if (json.frozen)
//            util.deepFreeze(json.obj);
//        return json.obj;
//    } */
//    else {
//        // Primitive
//        return json;
//    }
//}

// Deeply adds (im)mutability information
function addImmutabilityInfo(val : any) {
    console.log(`val is`);
    console.log(val);
    var copy = (typeof val === 'object') ? Object.assign(val) : val;
    var propNames = Object.getOwnPropertyNames(val);

    propNames.forEach(function(name) {
        copy[name] = addImmutabilityInfo(val[name]);
    });

    // Add info to ourself
    if (Object.isFrozen(val))
        return { frozen: true, val: copy };
    else
        return { frozen: false, val: copy };
}

/* Does the inverse of `addImmutabilityInfo`.
   Goes deeply through an object and makes every object immutable
   that was originally immutable. */
function reconstructImmutability(val : any) {
    console.log(`reconstructing`);
    console.log(val);
    if (!val.hasOwnProperty('frozen') || !val.hasOwnProperty('val'))
        throw new TypeError('Value lacks mutability information.');
    
    var propNames = Object.getOwnPropertyNames(val.val);
    propNames.forEach(name => {
        val.val[name] = reconstructImmutability(val.val[name]);
    });
    
    if (val.frozen)
        return Object.freeze(val.val);
    else
        return val.val;
}

// Adds mutability information to each value
/*
function replacer(val : any) {
    console.log('replacer called on: ');
    console.log(typeof val);
    console.log(val);
    
    if (Object.isFrozen(val))
        return { frozen: true, val: val };
    else
        return { frozen: false, val: val };
}

// Revives a value taking mutability into account
function reviver(json): any {
    if (json.hasOwnProperty('frozen')) {
        if (json.frozen)
            return Object.freeze(json.val);
        else
            return json.val;
    }
    else {
        throw new Error('Json lacks mutability information.');
    }
}*/

/*
// Serializes a value, incorporates type information needed for reconstruction in the `reviver`
function replacer(val : any) {
    console.log(`in`);
    if (typeof val === 'object') {
        console.log(`in1`);
        var type = val.constructor.name;
        console.log(`replacing ${type} by`);
        console.log({ type: type, value: val.toJSON() });
        if (!ExchangeableFactory.isExchangeable(type)) { 
            throw new TypeError(`Cannot serialize objects that are not register at the 'Exchangeable' factory.`); 
        }
        // stringify uses the object's toJSON method which is required by the `Exchangeable` interface
        return { type: type, value: val.toJSON() };
    }
    else if (typeof val === 'function') {
        console.log(`in2`);
        return { type: 'function', value: val.toString() };
    }
    else {
        console.log(`in3`);
        console.log(val);
        // Primitive
        return val;
    }
}*/

/*
function reviver(json): any {
    if (json.type) {
        if (json.type === 'function') {
            return eval(constructMethod(json.value));
        }
        else {
            // serializable object
            var Clazz = ExchangeableFactory.factory(json.type);
            return (new Clazz()).fromJSON(json.value);
        }
    }
    else {
        // Primitive
        return json;
    }
} */

function constructMethod(functionSource) {
    if (functionSource.startsWith("function")) {
        return "(" + functionSource + ")";
    }
    else {
        return "(function " + functionSource + ")";
    }
}