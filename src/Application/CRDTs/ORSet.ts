var replicaID, util;
export function setDependencies(ipAddress, utilities) {
    util      = utilities;
    replicaID = ipAddress;
}

/*    
NU ALS KLASSE MAKEN: 'ORSet' implements CRDT { ... }
    EN deze momenteel state based maken tot we weten hoe dat op-based exact werkt
    ik denk op-based: 
        - bij subscriben aan een service: krijgen we de hele service binnen inclusief de operaties van een EC 
        - daarna krijgen we operatie per operatie binnen wanneer die gebeuren (we krijgen die op zijn minst van diejen waarbij we hebben     gesubscribed ;) dus we zullen er geen missen :) )
        - Mss: operaties unique ID geven: ip + operatie counter (zodat we die geen 2 keer uitvoeren per ongeluk, extra safety)
*/

/*
 * Observed Remove Set:
 *   Internally we implement this set using 2 sets.
 *   A set of tagged additionss and a set of tagged deletions.
 *   As such, both sets can only grow, guaranteeing monotonicity.
 *   The price to pay is that a replica can't add an element it previously removed.
 *
 * IMPORTANT: Only primitive values are allowed in this set because the set can be replicated to multiple peers.
 *            Since the Set checks for equality using `===` objects cannot be used,
 *            because object !== replica(object).
 *            Implementing our own set that allows for object comparisons is not an option 
 *            because it would not achieve the performance that is expected from a Set.
 */

export class ORSet<T extends Primitive> implements CvRDT_I<ORSet<T>> {
    private addTags    : Map<T, Set<string>>;
    private removeTags : Map<T, Set<string>>;
    //private ops        : any; // contains operation executions
    
    constructor(addTags? : Map<T, string>, removeTags? : Map<T, string>) { //, ops? : any) {
        //this.ops        = ops ? ops : [];
        this.addTags    = addTags ? addTags : new Map();
        this.removeTags = removeTags ? removeTags : new Map();
    }
    
    // Update method `add` adds an element to the ORSet.
    add(elem : T): boolean {
        if (!this.addTags.has(elem)) {
            this.addTags.set(elem, new Set());
        }
        
        var addTags = this.addTags.get(elem);

        if (!addTags.has(replicaID)) {
            addTags.add(replicaID);
            //this.ops.push(['add', elem, replicaID]); // add the operation we just did to the list of executed operations
            return true;
        }
        
        return false;
    }
    
    // Update method `delete` removes an element from the ORSet.
    delete(elem : T): boolean {
        if (!this.addTags.has(elem) || !this.addTags.get(elem).has(replicaID)) {
            // We did not yet add it, hence we cannot remove it.
            return false;
        }
        
        if (!this.removeTags.has(elem)) {
            this.removeTags.set(elem, new Set());
        }
        
        var removeTags = this.removeTags.get(elem);
        if (!removeTags.has(replicaID)) {
            removeTags.add(replicaID);
            //this.ops.push(['delete', elem, replicaID]);
            return true;
        }
        
        return false;
	}
    
    /*
     * Query method `has` checks if an element is in the ORSet. The replica ID doesn't matter.
     * elem ∈ ORSet <==> (addTags \ removeTags) ≠ ∅
     */
	has(elem : T): boolean {
        // (a \ b)
        function setDifference(a, b) {
            let diff = new Set([...a].filter(x => !b.has(x)));
            return diff;
        }
             
        var addTags = this.addTags.has(elem) ? this.addTags.get(elem) : new Set();
        var removeTags = this.removeTags.has(elem) ? this.removeTags.get(elem) : new Set();
        var diff = setDifference(addTags, removeTags);
        return diff.size > 0;
	}

    /*
     * Destructive state-based merge: 
     *   for each element, let its add-tag list be the union of the two add-tag lists, and likewise for the two remove-tag lists.
     */
	merge(replica : ORSet<T>): void {
        function setUnion(a, b) {
            return new Set([...a, ...b]);
        }
        
        var allAddKeys = new Set([...this.addTags.keys(), ...replica.addTags.keys()]);
        var allRemovedKeys = new Set([...this.removeTags.keys(), ...replica.removeTags.keys()]);
        
        // for each element, let its add-tag list be the union of the two add-tag lists
        for (const key of allAddKeys) {
            var ourAddTags = this.addTags.has(key) ? this.addTags.get(key) : new Set();
            var replicaAddTags = replica.addTags.has(key) ? replica.addTags.get(key) : new Set();
            var union = setUnion(ourAddTags, replicaAddTags);
            
            this.addTags.set(key, union);
        }
        
        // for each element, let its removed-tag list be the union of the two removed-tag lists
        for (const key of allRemovedKeys) {
            var ourRemoveTags = this.removeTags.has(key) ? this.removeTags.get(key) : new Set();
            var replicaRemoveTags = replica.removeTags.has(key) ? replica.removeTags.get(key) : new Set();
            var union = setUnion(ourRemoveTags, replicaRemoveTags);
            
            this.removeTags.set(key, union);
        }
	}
    
    // Two ORSets are equal iff for all corresponding elements their add sets and remove sets are equal.
    equals(replica : ORSet<T>): boolean {
        return this.addTags.equals(replica.addTags) &&
               this.removeTags.equals(replica.removeTags);
    }
    
    tojson(): any {
        return [ this.addTags, this.removeTags ];
    }
    
    static fromjson<T extends Primitive> (json : any): ORSet<T> {
        const [ addTags, removeTags ] = json;
        return new ORSet(addTags, removeTags);
    }
}