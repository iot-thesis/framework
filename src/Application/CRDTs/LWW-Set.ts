var VectorClock, serialize, deserialize;

export function setDependencies(vectorClock, serializationModule) {
    VectorClock = vectorClock;
    serialize   = serializationModule.serialize;
    deserialize = serializationModule.deserialize;
}

/*
 * Implementation of an LWW-Element-Set (LWW = Last-Write-Wins).
 * API:
 *  - add(element)    --> boolean
 *  - remove(element) --> boolean
 *  - has(element)    --> boolean
 */

export class LWWSet<T extends Primitive> implements CmRDT_I<LWWSet<T>> {
    // Dictionary mapping an element to its add/remove set which is a set of timestamps.
    private _addSets    : Map<T, Set<VectorClock>>;
    private _removeSets : Map<T, Set<VectorClock>>;
    
    constructor(addSets? : Map<T, Set<VectorClock>>, removeSets? : Map<T, Set<VectorClock>>) {
        this._addSets    = addSets    ? addSets    : new Map();
        this._removeSets = removeSets ? removeSets : new Map();
    }
    
    /*
     * elem ∈ LWWSet <==> ∃c ∈ addTags: [∀c' ∈ removeTags: c' < c] where c and c' are timestamps (vector clocks)
     * This version is slightly different, it receives `c` as an argument and checks whether it happened after all removals.
     */
    has(elem : T, timestamp? : VectorClock): boolean {
        var {addSet, removeSet} = this.getSets(elem);
        
        if (!this._addSets.has(elem)) {
            return false;
        }
        else if (timestamp) {
            if ([...removeSet].find(vector => !VectorClock.happenedBefore(vector, timestamp))) {
                // ∃c' ∈ removeTags: c' ≮ c (where c is `timestamp` argument)
                return false;
            }
            else {
                return true;
            }   
        }
        else {
            /* Regular `has`.
               An element is in the LWW Set if it is in the remove set 
               but with an earlier timestamp than the latest timestamp in the add set. */
            var {addSet, removeSet} = this.getSets(elem);
            if ([...addSet].find(vc => this.happenedAfterAll(vc, removeSet)))
                return true;
            else
                return false;
        }
    }
    
    // Checks if a timestamp happened after all timestamps of a set
    private happenedAfterAll(timestamp : VectorClock, set : Set<VectorClock>) {
        for (var vc of set) {
            if (!VectorClock.happenedBefore(vc, timestamp)) {
                return false;
            }
        }
        return true;
    }
    
    add(elem : T, timestamp : VectorClock): void {
        // store a copy of the vector clock
        timestamp = deserialize(serialize(timestamp));
        this.addToSet(timestamp, elem, this._addSets);
    }
    
    remove(elem : T, timestamp : VectorClock): void {
        // store a copy of the vector clock
        timestamp = deserialize(serialize(timestamp));
        this.addToSet(timestamp, elem, this._removeSets);
    }
    
    private getSets(elem : T) {
        var addSet    = this._addSets.has(elem)    ? this._addSets.get(elem)    : new Set();
        var removeSet = this._removeSets.has(elem) ? this._removeSets.get(elem) : new Set();
        return { addSet: addSet, removeSet: removeSet };
    }
    
    private addToSet(timestamp : VectorClock, elem : T, sets : Map<T, Set<VectorClock>>): void {
        if (!sets.has(elem)) {
            sets.set(elem, new Set());
        }
        
        sets.get(elem).add(timestamp);
    }
    
    // Returns a copy of this LWW Set
    /*
    copy(): LWWSet<T> {
        function copyMapOfSets(mapSets) {
            var arrayCopy = Array.from(mapSets).map(([k, v]) => [k, Array.from(v)]); // transform map and sets to arrays
            return new Map(arrayCopy.map(([k, v]) => [k, new Set(v)])); // Copy of the map and sets
        }
        
        var setCopy         = new LWWSet();
        setCopy._addSets    = copyMapOfSets(this._addSets);
        setCopy._removeSets = copyMapOfSets(this._removeSets);
        
        return setCopy;
    }
    */
    
    /*
     * Implement the `tojson`, `fromjson` methods from the CmRDT_I interface.
     */
    
    tojson(): any {
        return [ this._addSets, this._removeSets ];
    }
    
    static fromjson<T extends Primitive> (json : any): LWWSet<T> {
        const [ addSets, removeSets ] = json;
        return new LWWSet<T>(addSets, removeSets);
    }
}