/*
 * This module exports the implemented CRDTs that are available for the users of our framework.
 * This module also creates the necessary factories such that we can reconstruct a received replica,
 * without requiring that class to be in the global scope (we can simply ask a reference to a given class at the factory).
 */

import      Factory        from '../factory';
import * as GMapModule     from './GMap';
import * as ORSetModule    from './ORSet';
import * as LWWSetModule   from './LWW-Set';
import      Conditions     from './SECRO/conditions';
import * as SECROModule    from './SECRO/SECRO';

const GMap     = GMapModule.GMap;
const SECRO    = SECROModule.SECRO;
const ORSet    = ORSetModule.ORSet;
const LWWSet   = LWWSetModule.LWWSet;

// Create an Exchangeable factory
const ExchangeableInterfaceMethods = ['tojson'];
const ExchangeableStaticMethods    = ['fromjson'];
const ExchangeableFactory          = new Factory('isExchangeable', ExchangeableInterfaceMethods, ExchangeableStaticMethods);

// Create CRDT factories and register the CRDTs
const CvRDTInterfaceMethods = ['merge', ...ExchangeableInterfaceMethods];
const CvRDTStaticMethods    = ExchangeableStaticMethods;
const CvRDTFactory          = new Factory('isCvRDT', CvRDTInterfaceMethods, CvRDTStaticMethods);

const CmRDTInterfaceMethods = ExchangeableInterfaceMethods;
const CmRDTStaticMethods    = ExchangeableStaticMethods;
const CmRDTFactory          = new Factory('isCmRDT', CmRDTInterfaceMethods, CmRDTStaticMethods);

// API for the end-user of our framework
const factoryAPI = { registerExchangeableClass: ExchangeableFactory.register.bind(ExchangeableFactory), 
                     registerCvRDTClass: function(clazz) {
                         // A CRDT is always serializable, hence also register it at the serializable factory
                         ExchangeableFactory.register.call(ExchangeableFactory, clazz);
                         CvRDTFactory.register.call(CvRDTFactory, clazz);
                     },
                     registerCmRDTClass: function(clazz) {
                         // A CRDT is always serializable, hence also register it at the serializable factory
                         ExchangeableFactory.register.call(ExchangeableFactory, clazz);
                         CmRDTFactory.register.call(CmRDTFactory, clazz);
                     }
                   };


factoryAPI.registerCvRDTClass(GMap);
factoryAPI.registerCvRDTClass(ORSet);
factoryAPI.registerCmRDTClass(LWWSet);

// Pass factories to modules that depend on it
function setDependencies(operationModule, serializationModule, ipAddress, VectorClock, util) {
    GMapModule.setDependencies(ExchangeableFactory, util);
    SECROModule.setDependencies(LWWSet, ExchangeableFactory, serializationModule, ipAddress, VectorClock, util, operationModule);
    ORSetModule.setDependencies(ipAddress, util);
    LWWSetModule.setDependencies(VectorClock, serializationModule);
}

export { ExchangeableFactory, CvRDTFactory, CmRDTFactory, factoryAPI, GMap, ORSet, LWWSet, SECRO, Conditions, setDependencies };