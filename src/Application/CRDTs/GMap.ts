/*
 * Implementation of a grow-only Map.
 * One can only add key-value pairs to this map.
 * Deletion of key-value pairs is not allowed.
 * Updating a key's value is not allowed either.
 *
 * Important: keys must be primitives because
 *            they are compared using the `===` operator,
 *            which does not work for replicated objects since `obj !== replica(obj)`
 */

var exchangeableFactory, util;
function setDependencies(exchangeableFactoryModule, utilities) {
    util = utilities;
    exchangeableFactory = exchangeableFactoryModule;
};

class GMap<K extends Primitive, V extends Eq<V>|Primitive> extends Map<K,V> implements CvRDT_I<GMap<K,V>> {
    // K and V must be primitives in order to be stringify'able and parsable
    constructor(it? : Iterable<any>) {
        if (it) {
            for (var [k, v] of it) {
                util.checkEqable(v, 'GMap values must implement the Eq interface.');
            }   
        }
        super(it);
    }
    
    // Provide own `set` method that has an etxra security check
    set(key : K, val : V): any {
        util.checkEqable(val, 'GMap values must implement the Eq interface.');

        /*
        if (util.isMutable(val))
            throw new TypeError(`Cannot set the value of '${key}' to a mutable object.`);*/
        if (this.has(key))
            throw new Error(`Cannot update the value of a key (${key}) in a GMap.`);
        else {
            super.set(key, val);
        }
    }
    
    // Prohibit deletion
    delete(key : K): boolean {
        throw new Error(`Cannot delete a key-value pair from a GMap.`);
    }
    
    // Destructive merge
    merge(replica : GMap<K, V>): void  {
        replica.forEach((value, key) => {
            if (!this.has(key)) {
                this.set(key, value);
            }
        });
    }
    
    static fromjson<K extends Primitive, V extends EqExchangeable<V>|Primitive> (json : any[]) : GMap<K, V> {
        return new GMap(json.map(([k, v]) => [k, util.deepFreeze(v)])); // make values immutable
    }
}

export { GMap, setDependencies };