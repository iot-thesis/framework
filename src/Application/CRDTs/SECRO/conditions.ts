/*
 * Representation of pre- and post-conditions.
 */

export default class Conditions implements Exchangeable<Conditions> {
    protected _conditions : Map<PropertyKey, (state : Object, ...args: any[]) => boolean>;
    
    constructor() {
        this._conditions = new Map();
    }
    
    has(e) {
        return this._conditions.has(e);
    }
    
    get(e) {
        return this._conditions.get(e);
    }
    
    // Adds a `validator` condition on `operation`.
    add(operation : PropertyKey, validator : (state : Object, ...args: any[]) => boolean): void {
        this._conditions.set(operation, validator);
    }
    
    tojson(): any {
        return this._conditions;
    }
    
    static fromjson(json): Conditions {
        var conditions         = new Conditions();
        conditions._conditions = json;
        return conditions;
    }
}