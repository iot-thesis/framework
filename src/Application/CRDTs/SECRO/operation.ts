/*
 * An operation is an object encapsulating:
 *  - `id`       : unique identifier for this operation
 *  - `name`     : name of the operation
 *  - `args`     : arguments passed to the operation
 *  - `clock`    : timestamp at which the operation happened, used to respect causal relations
 *  - `objectID` : ID of the object on which the operation took place
 */

var ipAddress, util;
var operationsCounter = 0;

export function setDependencies(ip, utilities) {
    ipAddress = ip;
    util = utilities;
}

export class Operation implements EqExchangeable<Operation> {
    readonly clock : VectorClock;
    public id : string;
    
    constructor(readonly name : PropertyKey, readonly args : any[], c : VectorClock, readonly objectID : UUID, readonly version: number, reconstruction? : boolean) {
        /* Only update the clock if we truly create a new operation.
           If we reconstruct a received operation, this is not a new operation and hence,
           our vector clock should not be updated! */
        if (!reconstruction) {
            c.update();
        }
        
        this.id = `${ipAddress}:${operationsCounter++}`; // unique id
        
        // Add a COPY of the clock to the operation (i.e. not a reference to the clock!)
        this.clock = c.copy();
    }
    
    execute(baseObject : Object) {
        var target = baseObject[this.name];
        if (!target || typeof target !== 'function') { return new TypeError(`${this.name} is not a function.`); }
        
        var res = target.apply(baseObject, this.args);
        return res;
    }
    
    equals(op : Operation): boolean {
        return this.id === op.id;
    }
    
    copy() {
        var copy = util.copyObject(this);
        copy.clock = this.clock; // because JSON stringify/parse cannot copy the vector clock
        return copy;
    }
    
    tojson(): any {
        return { name: this.name, args: this.args, clock: this.clock, id: this.id, objectID: this.objectID, version: this.version };
    }
    
    static fromjson(json : any): Operation {
        var {name, args, clock, id, objectID, version} = json;
        var immutableArgs = args.map(v => util.deepFreeze(v));
        var op = new Operation(name, immutableArgs, clock, objectID, version, true);
        op.id = id;
        operationsCounter--; // because instantiating the operation incremented the counter..
        return op;
    }
}