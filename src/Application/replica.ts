const spiders = require('spiders.js');

var Service, communication, CvRDTFactory, CmRDTFactory, SECRO, AsyncProxy, serialize, deserialize, ipAddress, util;
function setDependencies(serviceModule, CRDTModule, communicationModule, CvRDTFactoryModule, CmRDTFactoryModule, asyncProxy, serialization, ip, utilities) {
    Service       = serviceModule.Service;
    SECRO         = CRDTModule.SECRO;
    communication = communicationModule;
    CvRDTFactory  = CvRDTFactoryModule;
    CmRDTFactory  = CmRDTFactoryModule;
    AsyncProxy    = asyncProxy;
    serialize     = serialization.serialize;
    deserialize   = serialization.deserialize;
    ipAddress     = ip;
    util          = utilities;
}

/*
 * Implementation of a replica that wraps a base object.
 */

class Replica {
    public    name : string;
    protected _baseObject;
    private   _observer;
    private   _remoteObserver;

    constructor(name : string, obj: Object) {
        this.name            = name;
        this._observer       = false;
        this._remoteObserver = false;
        this.setBaseObject(obj);
    }
    
    getBaseObject() {
        return this._baseObject;
    }

    setBaseObject(o: Object) {
        this._baseObject = o;
    }

    /* Adds an observer to this CvRDT.
       Will be called everytime the CvRDT experiences an update.
       This can be a local update (calling a mutator) or a remote update (merging with a received replica). */
    onUpdate(observer) {
        if (typeof observer !== 'function') {
            throw new TypeError(`Observer must be a function but is of type ${typeof observer}.`);
        }
        this._observer = observer;
    }

    onRemoteUpdate(observer) {
        if (typeof observer !== 'function') {
            throw new TypeError(`Observer must be a function but is of type ${typeof observer}.`);
        }
        this._remoteObserver = observer;
    }

    // Notifies the observer (`onUpdate` listener) of a state change
    notifyObserver(newState = this.getBaseObject()) {
        if (this._observer) {
            this._observer(newState);
        }
    }

    // Notifies the remote observer (`onRemoteUpdate` listener) of a state change
    notifyRemoteObserver(newState = this.getBaseObject()) {
        if (this._remoteObserver) {
            this._remoteObserver(newState);
        }
    }
    
    /* Reconstructs a replica from its serialized representation.
       Nested objects that are not CRDTs are made immutable as must have been the case at the sender. */
    static fromReplica(owningService, name, replica) {
        if (replica.type === 'CvRDT') {
            //var crdt = util.deepFreezeNonCRDTs(deserialize(replica.baseObject), CvRDTFactory.isCvRDT, CmRDTFactory.isCmRDT);
            var crdt = deserialize(replica.baseObject);
            return CvRDT.fromReplica(owningService, name, crdt);
        }
        else if (replica.type === 'CmRDT') {
            //var crdt = util.deepFreezeNonCRDTs(deserialize(replica.baseObject), CvRDTFactory.isCvRDT, CmRDTFactory.isCmRDT);
            var crdt = deserialize(replica.crdt);
            return SECRO.fromReplica(owningService, name, crdt);
        }
        else if (replica.type === 'SC') {
            // Make a proxy that points to the owner of this strongly consistent object
            return StronglyConsistentProxy.fromReplica(owningService, name, replica);
        }
    }
}

/*
 * Implementation of an eventually consistent object.
 * A proxy controlling all accesses and operations is
 * automatically added as a property of the object's owning service.
 */

class CvRDT<V extends CvRDT_I<any>> extends Replica {
    constructor(name : string, val : V) {
        if (!val.constructor || !CvRDTFactory.isCvRDT(val.constructor.name)) { 
            throw new TypeError(`Cannot make a non-CRDT eventually consistent.`);
        }
        super(name, val);
    }
    
    replicate() {
        return { type: 'CvRDT', baseObject: serialize(this.getBaseObject()) };
    }
    
    static fromReplica(owningService, name, crdt) {
        var replica = owningService.makeCvRDT(name, crdt);
        return replica;
    }
    
    // Updates this CvRDT to the result of merging this CvRDT with the received replica
    merge(replica : CvRDT<V>): void { 
        var replicaOne = this.getBaseObject(), replicaTwo = replica.getBaseObject();
        if (typeof replicaOne !== typeof replicaTwo) { throw new TypeError("Cannot merge replicas of different types."); }

        if (replicaOne.constructor && CvRDTFactory.isCvRDT(replicaOne.constructor.name) &&
            replicaTwo.constructor && CvRDTFactory.isCvRDT(replicaTwo.constructor.name)) {
            if (replicaOne.constructor.name === replicaTwo.constructor.name) {
                // Replicas implement the "Mergeable" interface
                replicaOne.merge(replicaTwo); // destructive
    
                // The replica has been updated, execute the observers
                this.notifyObserver();
                this.notifyRemoteObserver();
            }
            else {
                throw new TypeError(`Cannot merge replicas of different types: ${replicaOne.constructor.name} and ${replicaTwo.constructor.name}`);
            }
        }
        else {
            throw new TypeError(`Cannot merge '${replicaOne.constructor.name}' since it is not a CRDT.`);
        }
    }
}

/*
 * CmRDTs are implemented as JSON CRDTs behind the scenes.
 * Therefore, the definition of a "CmRDT" is the `SECRO` in file `SECRO.ts`.
 */

/*
 * Implementation of a strongly consistent object,
 * which is a wrapper around the object.
 */

class StronglyConsistent extends Replica {
    constructor(baseObject) {
        super(null, AsyncProxy(baseObject));
    }
    
    getOwner() {
        return ipAddress;
    }
    
    replicate() {
        return { type: 'SC', owner: this.getOwner() };
    }
}

/*
 * Object that acts as a Strongly Consistent object.
 * However, it is a proxy to a remote object that is strongly consistent.
 * Hence, all accesses and operations on this proxy object will be performed through RPC.
 */

class StronglyConsistentProxy {
    _owner : string;
    constructor(name : string, owner : string) {
        this._owner = owner;
    }
    
    getOwner() {
        return this._owner;
    }
    
    replicate() {
        return { type: 'SC', owner: this.getOwner() };
    }
    
    static fromReplica(owningService, name, replica) {
        return owningService.makeSCProxy(name, replica.owner);
    }
}

export { setDependencies, Replica, CvRDT, StronglyConsistent, StronglyConsistentProxy }; //EventuallyConsistent,