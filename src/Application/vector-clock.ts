var ipAddress;

/*
 * Implementation of a dynamic vector clock.
 */

export function setDependencies(ExchangeableFactory, ip) {
    ipAddress = ip;
    ExchangeableFactory.register(VectorClock);
}

export class VectorClock implements Exchangeable<VectorClock> {
    private _ownID  : UUID;
    private _vector : VC;
    
    constructor() { //ownID : UUID) {
        this._ownID  = ipAddress; //ownID;
        this._vector = new Map();
        this._vector.set(this._ownID, 0); // initialize our own clock on zero
    }
    
    get(): VC {
        return this._vector;
    }
    
    // Overloaded update method
    update(): VC;
    update(vector : VectorClock): VC;
    update(vector?): VC {
        // Update our own clock in the "vector"
        var ourClock = VectorClock.getVectorEntry(this._ownID, this._vector);
        this._vector.set(this._ownID, ourClock + 1);
        
        if (vector) {
            // Update each entry to the maximum of both vectors
            var myEntries       = this._vector.keys();
            var receivedEntries = vector.get().keys();
            var allEntries      = new Set([...myEntries, ...receivedEntries]);

            allEntries.forEach(entry => {
                var ownClock      = VectorClock.getVectorEntry(entry, this._vector);
                var receivedClock = VectorClock.getVectorEntry(entry, vector.get());

                this._vector.set(entry, Math.max(ownClock, receivedClock));
            });
        }
        
        return this._vector;
    }
    
    // Returns the clock's value of the vector entry with the given id
    static getVectorEntry(id : UUID, vector : VC): Clock {
        var clock = vector.has(id) ? vector.get(id) : 0;
        return clock;
    }
    
    /*
     * Algorithm: https://en.wikipedia.org/wiki/Vector_clock
     * VC(x) denotes the vector clock of event x, and VC(x)_{z} denotes the component of that clock for process z.
     * VC(x) < VC(y) <==> ∀z[VC(x)_{z} <= VC(y)_{z}] ∧ ∃z'[VC(x)_{z'} < VC(y)_{z'}]
     */
    static happenedBefore(vector : VectorClock, otherVector : VectorClock): boolean {
        var myEntries       = vector.get().keys();
        var receivedEntries = otherVector.get().keys();
        var allEntries      = new Set([...myEntries, ...receivedEntries]);
        
        var foundSmallerOne = false; // condition that at least one entry must be strictly smaller
        for (var it = allEntries.values(), entry = null; entry = it.next().value;) {
            var ownClock      = VectorClock.getVectorEntry(entry, vector.get());
            var receivedClock = VectorClock.getVectorEntry(entry, otherVector.get());
            if (receivedClock < ownClock) {
                return false;
            }
            else if (ownClock < receivedClock) {
                foundSmallerOne = true;
            }
        }
        
        return foundSmallerOne;
    }
    
    // x and y are concurrent iff VC(x) ≮ VC(y) ∧ VC(y) ≮ VC(x)
    static isConcurrent(vector: VectorClock, otherVector: VectorClock): boolean {
        return !VectorClock.happenedBefore(vector, otherVector) &&
               !VectorClock.happenedBefore(otherVector, vector);
    }
    
    // Returns a copy of this vector clock
    copy() : VectorClock {
        var copy = new VectorClock();
        copy._vector = new Map(Array.from(this._vector));
        return copy;
    }
    
    tojson(): any {
        return this._vector;
    }
    
    static fromjson(vector : any): VectorClock {
        var vc = new VectorClock();
        vc._vector = vector;
        return vc;
    }
}