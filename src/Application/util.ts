/*
 * Utility functions
 */

var Conditions, serialize, deserialize, CvRDTFactory, CmRDTFactory, Replica;
export function setDependencies(conditions, serializationModule, CvRDTFactoryModule, CmRDTFactoryModule, replicaModule) {
    serialize    = serializationModule.serialize;
    deserialize  = serializationModule.deserialize;
    Conditions   = conditions;
    CvRDTFactory = CvRDTFactoryModule;
    CmRDTFactory = CmRDTFactoryModule;
    Replica      = replicaModule.Replica;
}

export function equals(v1, v2) {
    if (implementsEqable(v1) && implementsEqable(v2)) {
        // Use their `equals` method
        if (!v1.equals(v2)) {
            return false;
        }
    }
    else {
        // Use default `===` comparison
        if (v1 !== v2) {
            return false;
        }
    }
    
    return true;
}

/* Returns the type of a value,
   if it is an object its returns the name of its 'class'. */
export function getType(val) {
    if (typeof val === 'object') {
        return val.constructor.name;
    }
    else {
        return typeof val;
    }
}

// Reconstructs a value based on the type information
export function reconstructValue(type : string, val, exchangeableFactory) {
    if (exchangeableFactory.isExchangeable(type)) {
        var Clazz = exchangeableFactory.factory(type);
        return Clazz.fromjson(val);
    }
    else {
        // it's a primitive type
        return val;
    }
}

export function checkType(val, exchangeableFactory) {
    if (typeof val === 'object') {
        // it's not a primitive, hence, it must be exchangeable
        if (!exchangeableFactory.isExchangeable(getType(val))) {
            throw new TypeError(`${val.constructor.name} does not implement the 'Exchangeable<${val.constructor.name}>' interface.`);
        }
    }
}

/* 
 * Input: A list of keys and their values
 * Output: A list of tagged keys and values. 
 */
/*
export function tagKeyValues(kvArray) {
    return kvArray.map(([key, value]) => {
        return { keyType: getType(key), valueType: getType(value), key: key, value: value };
    });
} */

/* 
 * Reconstructs the list of key values from a list of tagged keys and values.
 * Hence, this is the inverse of `tagKeyValues`, i.e. `reconstructKeyValues(tagKeyValues(arr))` equals `arr`.
 */
/*
export function reconstructKeyValues(taggedKeyVals, exchangeableFactory) {
    return taggedKeyVals.map(({ keyType, valueType, key, value }) => {
        var reconstructedKey = reconstructValue(keyType, key, exchangeableFactory);
        var reconstructedVal = reconstructValue(valueType, value, exchangeableFactory);
        return [reconstructedKey, reconstructedVal];
    });
}; */

// Taken from: https://stackoverflow.com/questions/31054910/get-functions-methods-of-a-class
export function getAllMethods(obj, stopAt?) {
    let keys = [];
    let topObject = obj;

    const onlyOriginalMethods = (p, i, arr) =>
        typeof topObject[p] === 'function' &&  // only the methods
        p !== 'constructor' && // not the constructor
        (i === 0 || p !== arr[i - 1]) && // not overriding in this prototype
        keys.indexOf(p) === -1 // not overridden in a child

    do {
        const l = Object.getOwnPropertyNames(obj)
        //.concat(Object.getOwnPropertySymbols(obj).map(s => s.toString()))
            .sort()
            .filter(onlyOriginalMethods)
        keys = keys.concat(l)

        // walk-up the prototype chain
        obj = Object.getPrototypeOf(obj)
    } while (obj !== stopAt && // stop at the requested prototype
    Object.getPrototypeOf(obj)); //not the the Object prototype methods (hasOwnProperty, etc...))

    return keys;
}

/*
 * Deep-copies an `Exchangeable` object.
 */
export function copyObject(obj : Exchangeable<any>) {
    return deserialize(serialize(obj));
}

/*
 * Returns an array of all methods that have names starting with "get".
 */
export function getMethodsStartingWith(prefix, obj) {
    return getAllMethods(obj).filter(prop => typeof prop === 'string' && prop.startsWith(prefix));
}


export function getPreconditions(obj) {
    return getConditions('pre', obj);
}

export function getPostconditions(obj) {
    return getConditions('post', obj);
}

// type must be `pre` or `post`
function getConditions(type, object) {
    var conditions  = new Conditions();
    var originalObj = object.constructor.prototype.constructor; // contains the static methods (pre and postconditions must be static)
    
    var staticMethods = [];
    var obj = originalObj;

    do {
        const l = Object.getOwnPropertyNames(obj)
            .forEach((p, i, arr) => {
                if (typeof obj[p] === 'function'    &&  //only the methods
                    p !== 'constructor'             &&  //not the constructor
                    (i == 0 || p !== arr[i - 1])    &&  //not overriding in this prototype
                    staticMethods.indexOf(p) === -1 &&  //not overridden in a child
                    p.startsWith(type))
                {
                    
                    var noPre         = p.slice(type.length); // cut off the type (which is `pre` or `post`)
                    var operationName = noPre[0].toLowerCase() + noPre.slice(1); // lower the first letter
                    var validator     = obj[p];
                   
                    if (!object[operationName] || typeof object[operationName] !== 'function') {
                        throw new TypeError(`Found a ${type}condition '${name}' but no method '${operationName}'. Please follow the naming convention for pre and post conditions.`);
                    }
                   
                    conditions.add(operationName, validator);
                }
            });
        
        staticMethods = staticMethods.concat(l)
    }
    while ((obj = Object.getPrototypeOf(obj)) && //walk-up the prototype chain
           Object.getPrototypeOf(Object.getPrototypeOf(obj)))
    
    return conditions;
}

/* Deep checks an object on immutability.
   Returns true if the object or a nested object is mutable and not a CRDT. */
//export function isMutable(obj : Object): boolean {
    /* IN FUNCTIONAL STYLE, BUT goes twice through all properties... of the object (once in filter and once in forEach...)
    Object.getOwnPropertyNames(obj)
          // Retain objects that are not CRDTs
          .filter(name => typeof obj[name] === 'object' && obj[name] !== null && 
                          !CvRDTFactory.isCvRDT(obj.constructor.name) && 
                          !CmRDTFactory.isCmRDT(obj.constructor.name))
          .forEach(name => {
              if (!Object.isFrozen(obj[name]))
                  throw new TypeError('CRDT contains mutable nested objects other than CRDTs. Only CRDTs or immutable objects can be nested within other CRDTs.');
              else
                  containsImmutableNonCRDTs(obj[name]);
    }); 
    return false;
    */
    /*
    if (!Object.isFrozen(obj) && // primitives are always frozen
        !CvRDTFactory.isCvRDT(obj.constructor.name) && 
        !CmRDTFactory.isCmRDT(obj.constructor.name)) {
        return true;
    }
    
    var props = Object.getOwnPropertyNames(obj);
    for (var name of props) {
        if (typeof obj[name] === 'object'  && obj[name] !== null && 
            !CvRDTFactory.isCvRDT(obj[name].constructor.name) && !CmRDTFactory.isCmRDT(obj[name].constructor.name))
        {
            console.log(`${name} is not a CRDT`);
            // Object is not a CRDT --> must be immutable
            if (!Object.isFrozen(obj[name]) || isMutable(obj[name]))
                return true;
        }
    }
    
    return false;
}
*/

/* Deep freezes an object and its nested objects.
   Every nested object that is not a CRDT will be made immutable. */
export function deepFreeze(obj) {
    var props = (obj && typeof obj === 'object') ? Object.getOwnPropertyNames(obj) : [];
    for (var name of props) {
        if (typeof obj[name] === 'object' && obj[name] !== null && 
            !CvRDTFactory.isCvRDT(obj.constructor.name) && !CmRDTFactory.isCmRDT(obj.constructor.name))
        {
            deepFreeze(obj[name]);
        }
    }
    
    return Object.freeze(obj);
}

export function implementsEqable(obj) {
    return obj && typeof obj === 'object' && obj.equals && typeof obj.equals === 'function';
}

export function checkEqable(val, errorMsg) {
    if (typeof val === 'object' && !implementsEqable(val))
        throw new TypeError(errorMsg);
}

/*
 * Returns the index at which the element was found.
 * If it is not found, it returns the index at which it may be inserted.
 */
export function binarySearch(elem, array, lesserThen : (x : any, y: any) => boolean) {
    function helper(start : number, end : number) {
        if (array.length === 0) {
            return { found: false, index: 0 };
        }
        else if (lesserThen(array[end], elem)) {
            return { found: false, index: end+1 };
        }
        else if (lesserThen(elem, array[start])) {
            return { found: false, index: start-1 };
        }
        else if (start === end) {
            // There is only one element E anymore and elem ≮ E and E ≮ elem, hence elem == E
            return { found: true, index: start };
        }
        else {
            var first = array[start];
            var last  = array[end];
            var mid   = Math.floor((end - start) / 2);
            
            if (lesserThen(array[mid], elem)) {
                // Insert into second half
                return helper(mid+1, end);
            }
            else if (lesserThen(elem, array[mid])) {
                // Insert into first half
                return helper(start, mid-1);
            }
            else {
                // array[mid] equals elem
                return { found: true, index: mid };
            }
        }
    }
    
    return helper(0, array.length - 1);
}

/*
 * Efficient permutation algorithm in O(n).
 * Taken from: https://stackoverflow.com/questions/9960908/permutations-in-javascript
 */
export function* permute(permutation) {
  var length = permutation.length,
      c = Array(length).fill(0),
      i = 1, k, p;

  yield permutation.slice();
  while (i < length) {
    if (c[i] < i) {
      k = i % 2 && c[i];
      p = permutation[i];
      permutation[i] = permutation[k];
      permutation[k] = p;
      ++c[i];
      i = 1;
      yield permutation.slice();
    } else {
      c[i] = 0;
      ++i;
    }
  }
}

/*
 * Wraps the given value in a promise that is immediately resolved.
 */

export function wrapInPromise(val : any) {
    return new Promise(resolve => resolve(val));
}

/*
export function toJS(val) {
    if (Immutable.isCollection(val))
        return val.toJS();
    else
        return val;
}
*/

/*
 * Returns a boolean indicating whether the given value is a replica or not.
 */

export function isReplica(val : any) {
    if (typeof val !== 'object' || val === null)
        return false;
    else
        return CvRDTFactory.isCvRDT(val.constructor.name) ||
               CmRDTFactory.isCmRDT(val.constructor.name);
}