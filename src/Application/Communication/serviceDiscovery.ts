const spiders = require('spiders.js');
var applicationModule, serviceModule, replicaModule, lan, serialize, deserialize, ipAddress, SECRO;

/*
 * This communication module relies on underlying communication plugins.
 * Every communication plugin must implement the following interface:
 *   broadcast(String message)
 *   onMessage(Function handler)
 *   makeFarRef(String identifier)
 */

function setDependencies(applicationMod, serviceMod, replicaMod, lanModule, serializationModule, ip, SECROModule) {
    ipAddress         = ip;
    applicationModule = applicationMod;
    serviceModule     = serviceMod;
    replicaModule     = replicaMod;
    serialize         = serializationModule.serialize;
    deserialize       = serializationModule.deserialize;
    lan               = lanModule;
    SECRO             = SECROModule;
    
    lan.onMessage(onMessage);
    startDiscovery(); // Start multicasting our presence on the network
}

var msgCtr = 1;
const subscriptions  = new Map(); // maps a service type to a callback
const seenUpdates    = new Map(); // stores IDs of received updates
const seenOperations = new Map(); // Stores IDs of received operations

/*
 * Each peer can publish services and subscribe to services of a given type.
 * When we publish a service we send the service to all our subscribers for that typetag.
 * We also register ourself as a subscriber of that service because we must receive updates of this service.
 *
 * When we subscribe to a service we register ourself as a subscriber at all peers.
 * As such, we get the services of interest and will be notified when someone publishes a service we subscribed to.
 *
 * Hence, this is the implementation of a service discovery technique.
 * If broadcasting refers to "multicast UDP" on the network than this technique is called "Searching on my network",
 * if it happens via BLE, NRF, etc than it is called "Searching nearby",
 * as discussed by Bonnet et al in "A Categorization of Discovery Technologies for the Internet of Things".
 */

//const publishers = new Map(); // far refs to remote peers who published services to us (indexed on typetag)
const subscribers = new Map(); // far refs to remote peers who subscribed to one of our service type tags
const publishedServicesByType = new Map(); // published services, indexed on type tag

function startDiscovery() {
    // Broadcast our identity every 10 seconds, because UDP is unreliable
    const discoveryMessage = new Buffer(JSON.stringify({ app: '__CS__', ip: ipAddress }));
    lan.broadcast(discoveryMessage);
    setInterval(() => lan.broadcast(discoveryMessage), 10000);
}

// Processes incoming broadcast messages
function onMessage(msg, sender) {
    const parsed = JSON.parse(msg);
    if (!parsed.app || !parsed.ip)
        console.log("[WARNING]: UDP message not understood.");

    const ip = parsed.ip;

    if (ip !== ipAddress && !lan.hasFarRef(ip)) {
        console.log(`Connecting with: ${ip}`);
        // Setup a far reference with the discovered peer
        const ref = makeFarRef(ip);

        ref.then(a => console.log(`connected with ${ip}`));

        subscriptions.forEach((cb, typeTag) => {
            // Subscribe to the typetag at the peer
            ref.then(farRef => farRef.registerSubscriber(typeTag, ipAddress))
               .then(services => services.forEach(onService))
               .catch(e => console.log(`Subscribing to ${typeTag} at remote host ${ip} failed: ${e}.`));
        });
    }
}

// Publishes a service by sending it directly to all subscribers of that typetag
function publishService(service) {
    const publishedServices = publishedServicesByType.getOrElse(service._TYPETAG_, []);
    const alreadyExported   = publishedServices.find(s => s._ID_ === service._ID_);

    if (!alreadyExported) {
        // Add the service to the index of published services
        publishedServices.push(service);
        publishedServicesByType.set(service._TYPETAG_, publishedServices);

        // Send the service to all subscribers of that type tag
        sendToSubscribers(service);

        // Also subscribe to this typetag, since we must get updates for our published service
        subscribe(service._TYPETAG_, false);
    }
}

// Register ourself as a subscriber at all peers
function subscribe(typeTag, callback) {
    if (!subscriptions.has(typeTag)) {
        if (callback !== false)
            subscriptions.set(typeTag, callback); // store callback

        // Subscribe at all peers
        lan.getAllFarRefs()
           .forEach(peer =>
               peer.then(farRef => farRef.registerSubscriber(typeTag, ipAddress))
                   .then(services => services.forEach(onService))
                   .catch(e => console.log(`Subscribing to ${typeTag} at remote host failed: ${e}.`)));
    }
}

// Sends a service to all peers that are subscribed to the type tag of that service
function sendToSubscribers(service) {
    var subs = subscribers.getOrElse(service._TYPETAG_, []);
    subs.forEach(sub => sub.then(farRef => farRef.receiveService(service.toString()))
                           .catch(e => console.log(`Failed to deliver service: ${e}.`)));
}

function broadcastUpdate(updateID, service) {
    // Generate a unique update ID
    var updateID = updateID ? updateID : `${ipAddress}-U${msgCtr++}`;
    seenUpdates.set(updateID, true); // mark update as seen since we could be the originator
    
    lan.getAllFarRefs().forEach(peer => peer.then(farRef => farRef.receiveUpdate(updateID, service.toString()))
                                            .catch(e => console.log(`Failed to deliver update: ${e}.`)));
}

function broadcastOperation(serviceID : string, replicaName : PropertyKey, operation : Operation) {
    seenOperations.set(operation.id, true);
    var infoMessage = JSON.stringify({ serviceID: serviceID, name: replicaName, op: serialize(operation) });
    lan.getAllFarRefs().forEach(peer => peer.then(farRef => farRef.receiveOperation(infoMessage))
                                            .catch(e => console.log(`Failed to deliver operation: ${e}.`)));
}

// Someone wants to subscribe to one of our services types
function newSubscriber(typeTag, ip) {
    // Add the peer to our subscribers
    var farRef = lan.makeFarRef(ip);
    var farRefs = subscribers.getOrElse(typeTag, []);

    if (!farRefs.includes(farRef)) {
        farRefs.push(farRef);
    }
    subscribers.set(typeTag, farRefs);

    // Send him all services of that type tag that we previously exported (or received)
    var publishedServices = publishedServicesByType.getOrElse(typeTag, []).map(service => service.toString());
    return publishedServices;
}

function onService(receivedService) {
    receivedService = serviceModule.Service.fromString(receivedService);
    
    if (subscriptions.has(receivedService._TYPETAG_)) {
        // Discovered a service we're subscribed to.
        var knownService = serviceModule.getService(receivedService._ID_);
        
        if (knownService) {
            /* We already have a replica of this service.
               Merge the corresponding eventually consistent properties. */
            mergeServices(knownService, receivedService);
        }
        else {
            // New service
            serviceModule.addService(receivedService);
            var typetag = receivedService._TYPETAG_;
            receivedService._TYPETAG_ = null; // else it will be treated as "already published"
            receivedService.publish(typetag); // for transitivity
            
            try {
                subscriptions.get(receivedService._TYPETAG_)(receivedService); // execute the registered callback   
            } catch (e) {
                throw new Error(`Error in callback on ${receivedService._TYPETAG_}: ${e}`);
            }
        }
    }
}

// Merges corresponding CvRDT replicas upon receiving an update
function onUpdate(updateID, receivedService) {
    receivedService = serviceModule.Service.fromString(receivedService);
    
    if (!seenUpdates.has(updateID)) {
        // This is a new update
        seenUpdates.set(updateID, true); // mark it as seen
        
        // Broadcast update to everyone
        broadcastUpdate(updateID, receivedService);
        
        /* Check if we have this service, if we do,
           merge the corresponding CvRDT replicas. */
        var localReplica = serviceModule.getService(receivedService._ID_);
        if (localReplica) {
            mergeServices(localReplica, receivedService);
        }
        else {
            console.log('[WARNING]: Oops, unknown service update.');
        }
    }
}

// Informs the corresponding CmRDT of the received operation
function onOperation(operationInfo) {
    var {serviceID, name, op} = operationInfo;
    var operation = deserialize(op);
    
    if (!seenOperations.has(operation.id)) {
        // This is a new operation
        seenOperations.set(operation.id, true);
        
        // Broadcast operation to everyone
        broadcastOperation(serviceID, name, operation);
        
        /* Check if we have this service, if we do,
           inform the corresponding CmRDT of this operation. */
        var localReplica = serviceModule.getService(serviceID);
        if (localReplica) {
            localReplica._REPLICAS_[name].receive(operation);
        }
        else {
            console.log('[WARNING]: Oops, unknown service.');
        }
    }
}

/*
 * Merges a local service with a received service.
 * Hence, it iterates through all replicas of the received service
 * and merges the eventually consistent replicas with the corresponding
 * replicas of the local replica of this service.
 */
function mergeServices(localService, receivedService) {
    Object.keys(receivedService._REPLICAS_)
          .map(key => receivedService._REPLICAS_[key])
          .forEach(replica => {
              var replicas = localService._REPLICAS_[replica.name];
              if (replicas) {

                  if (replica instanceof replicaModule.CvRDT) {
                      // Merge the corresponding ECs
                      replicas.merge(replica);
                  }
                  else if (replica instanceof SECRO) {
                      // Merge the operation histories
                      replicas.mergeHistory(replica.getHistory());
                  }
              }
              else {
                  // Replica is missing
                  if (replica instanceof replicaModule.CvRDT || replica instanceof SECRO)
                      console.log("[ERROR]: Replicated services should have the same replicas but they haven't.");
              }
          });
}

function makeFarRef(ip) {
    return lan.makeFarRef(ip);
}

export { setDependencies, publishService, subscribe, newSubscriber, sendToSubscribers, broadcastUpdate, broadcastOperation, makeFarRef, onService, onUpdate, onOperation };