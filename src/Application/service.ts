//import * as Immutable from 'immutable';

const spiders = require('spiders.js');
var Replica, CvRDT, CmRDT, StronglyConsistent, StronglyConsistentProxy, CvRDTFactory,
    CmRDTFactory, communication, broadcastOperation, broadcastUpdate, lan, serialize, deserialize, SECRO, util, ipAddress;

function setDependencies(replicaModule, communicationModule, lanModule, CRDTFactories, serializationModule, secro, utilities, ip) {
    Replica                 = replicaModule.Replica;
    CvRDT                   = replicaModule.CvRDT;
    CmRDT                   = replicaModule.CmRDT;
    StronglyConsistent      = replicaModule.StronglyConsistent;
    StronglyConsistentProxy = replicaModule.StronglyConsistentProxy;
    communication           = communicationModule;
    CvRDTFactory            = CRDTFactories.CvRDTFactory;
    CmRDTFactory            = CRDTFactories.CmRDTFactory;
    lan                     = lanModule;
    serialize               = serializationModule.serialize;
    deserialize             = serializationModule.deserialize;
    SECRO                   = secro;
    util                    = utilities;
    ipAddress               = ip;
    broadcastOperation      = communication.broadcastOperation;
    broadcastUpdate         = communication.broadcastUpdate;
}

var serviceCtr = 1; // for generating unique IDs
var services = new Map(); // own services, indexed on ID

function getService(ID) {
    return services.get(ID);
}

function addService(service) {
    if (!services.has(service._ID_)) {
        services.set(service._ID_, service); // indexation by ID
    }
}

// Overwrites the broadcasting mechanism to be used
function setBroadcast(bc: (serviceID : string, replicaName : PropertyKey, operation : Operation) => void) {
    broadcastOperation = bc;
}

/*
 * Implementation of a service, which is a collection of
 * eventually/strongly consistent replicas.
 */

class Service {
    _ID_ : string;
    _TYPETAG_ : string;
    _REPLICAS_: any;
    
    /* 
     * The constructor defines some "private" properties
     * that can only be accessed with their symbol.
     */
    constructor(typeTag? : string, id? : string) {
        if(typeTag && typeof typeTag !== 'string') { throw new TypeError(`Service type tag must be a string.`); }
        this._ID_ = id ? id : `${ipAddress}:S${serviceCtr++}`; // unique service ID
        this._TYPETAG_ = typeTag; // service will be published under this tag
        
        /*
         * DCT of names to the corresponding replica.
         * This is needed in order to be able to access the meta replica object instead of its underlying (wrapped) base object.
         * i.e. to access a 'StronglyConsistent', 'EventuallyConsistent' or 'StronglyConsistentProxy' object.
         */
        this._REPLICAS_ = [];

        if (!id) {
            // The service was received. The receiving function will add the service.
            addService(this);
        }
    }

    /*
     * Makes a replicated datatype by delegating to one of `makeCvRDT`, `makeCmRDT` or `makeSC`.
     * The consistency model (strong consistency or strong eventual consistency) is determined based on the passed object.
     * An object is a CRDT if it implements either the `CvRDT_I` or the `CmRDT_I` interface.
     * CRDTs use the strong eventual consistency model, other regular objects use the strong consistency model.
     */
    makeReplica<V>(name : string, baseObj : any) { // id is needed for the simulator
        if (!broadcastOperation) broadcastOperation = communication.broadcastOperation;
        if (typeof name !== 'string') {
            throw new TypeError("Replica's name must be a string.");
        }
        else if (typeof baseObj !== 'object') {
            throw new TypeError(`Cannot make a replica out of a non-object of type ${typeof baseObj}`);
        }
        else if (CvRDTFactory.isCvRDT(baseObj.constructor.name)) {
            return this.makeCvRDT(name, <CvRDT_I<V>>baseObj);
        }
        else if (CmRDTFactory.isCmRDT(baseObj.constructor.name)) {
            return this.makeCmRDT(name, <CmRDT_I<V>>baseObj);
        }
        else {
            // `baseObj` is a regular object, hence, it must use the strong consistency model
            return this.makeSC(name, baseObj);
        }
    }
    
    // Makes a state-based CRDT
    makeCvRDT<V>(name : string, baseObj : CvRDT_I<V>) {
        var replica = new CvRDT(name, baseObj);
        this._REPLICAS_[name] = replica;
        
        /* Proxy delegates all calls to the base level object.
           When the base object is mutated, we send the new state to all subscribers. */
        return new Proxy(replica, delegateToBaseObject(this, name));
    }
    
    /*
     * Makes an op-based CRDT.
     * Operation based CRDTs (i.e. CmRDTs) are implemented using the JSON CRDT behind the scenes.
     */
    makeCmRDT<V>(name : string, baseObj: CmRDT_I<V>, crdt? : SECRO) {
        /*
         * Accessors are marked in the CmRDT using the `@accessor` decorator.
         * Preconditions and postconditions are extracted from the CmRDT "by convention".
         * Preconditions and postconditions should start with "pre" and "post" followed by the name of the operation.
         * An example: precondition for an `insertAfter` operation is `preInsertAfter` --> note the capital I
         * Note: every operation that has pre and/or postconditions should start with a lower case letter!
         */

        var accessors      = crdt ? null : [];
        var preconditions  = crdt ? null : util.getPreconditions(baseObj),
            postconditions = crdt ? null : util.getPostconditions(baseObj);

        if (!crdt && baseObj.constructor.hasOwnProperty('accessors')) {
            accessors = Array.from((<any>baseObj.constructor).accessors);
        }

        var replica = crdt ? crdt : new SECRO(name, baseObj, accessors, preconditions, postconditions);
        this._REPLICAS_[name] = replica;

        return new Proxy(replica, CmRDTHandler(this, name));
    }
    
    // Makes a strongly consistent object
    makeSC(name : string, baseObj : any) {
        var replica = new StronglyConsistent(baseObj);
        this._REPLICAS_[name] = replica;
        return new Promise(resolve => resolve(replica.getBaseObject()));
    }
    
    // Makes a proxy to a remote strongly consistent object
    makeSCProxy(name : string, owner : string) {
        if (typeof name !== 'string') { throw new TypeError("Replica's name must be a string."); }
        if (typeof owner !== 'string') { throw new TypeError("Owner must be a string (ip-adress)."); }
        
        var replica = new StronglyConsistentProxy(name, owner);
        this._REPLICAS_[name] = replica;
        
        var that = this;
        return lan.makeFarRef(owner)
               .then(farRef => farRef.getServiceField(that._ID_, name))
               .catch(e => console.log(`Far reference error: ${e}`));
    }
    
    /*
     * Publishes the service by sending it to its direct neighbours.
     * Static in order not to have name clashes with a possible user-defined `publish` function.
     */
    publish(typeTag : string) {
        if (!this._TYPETAG_) {
            this._TYPETAG_ = typeTag;
            communication.publishService(this);
            return this;
        }
        else {
            throw new Error(`Cannot republish ${this.constructor.name} service.`);
        }
    }
    
    // Subscribes to services of the given type tag.
    static subscribe(typeTag, callback) {
        if(typeof typeTag !== 'string') { throw new TypeError(`Service type tag should be a string but is a ${typeof typeTag}.`); };
        if (typeof callback !== 'function') { 
            throw new TypeError(`Second argument to subscribe must be a function, but got a ${typeof callback}.`);
        }
        
        return communication.subscribe(typeTag, callback);
    }
    
    /*
     * `toString` transforms a service into its string representation.
     * All properties are tagged such that we can differentiate between
     * a regular (user-defined) property and one that represents a replica.
     */
    toString() {
        // Make a copy of this service, do not copy the meta-property '_REPLICAS_'
        var serviceCopy = {};
        Object.getOwnPropertyNames(this)
              .filter(name => name !== "_REPLICAS_")
              .concat(util.getAllMethods(this, Service.prototype))
              .forEach(name => {
                  var val = this[name];
                  var replica = this._REPLICAS_[name];
                  var taggedValue;
            
                  if (replica) {
                      // A value container that represents this replica
                      taggedValue = { tag: "_REPLICA_", value: replica.replicate() };
                  }
                  else {
                      /*
                       * Strongly/Eventually consistent data MAY NOT be nested within regular (non-consistent) data.
                       * This would make no sense since regular data is not kept consistent.
                       */
                      if (typeof val === 'function') {
                          taggedValue = { tag: "_FUNCTION_", value: serialize(val) }
                      }
                      else {
                          taggedValue = { tag: "_REGULAR_VALUE_", value: val};  
                      }
                  }
                  serviceCopy[name] = JSON.stringify(taggedValue);
              });
        
        var serialized = JSON.stringify(serviceCopy);
        return serialized;
    }
    
    /*
     * Reconstructs a service from its string representation.
     * Untags the properties and reconstructs its value. 
     */
    static fromString(json) {
        var serviceObj = JSON.parse(json);
        
        // Make a "real" Service out of the parsed string
        const typetag = JSON.parse(serviceObj._TYPETAG_).value,
              id = JSON.parse(serviceObj._ID_).value;
        var service = new Service(typetag, id);
        
        // Copy properties into the newly defined service
        Object.getOwnPropertyNames(serviceObj)
              .forEach(name => {
                  var taggedVal = JSON.parse(serviceObj[name]);
              
                  switch(taggedVal.tag) {
                       case "_REPLICA_":
                           service[name] = Replica.fromReplica(service, name, taggedVal.value);
                           break;
                       case "_FUNCTION_":
                           service[name] = deserialize(taggedVal.value);
                           break;
                       case "_REGULAR_VALUE_":
                           service[name] = taggedVal.value;
                           break;
                  }
              });
        return service;
    }
}

/*
 * Delegates all calls to the base object of a replica.
 * When the object is mutated, the new state is sent to all subscribers.
 * This is the handler for the proxy that is used for CvRDTs (i.e. state-based CRDTs).
 *
 * In order to unify the APIs of both consistency models (strong consistency and strong eventual consistency),
 * accessing fields or invoking methods on a replicated datatype returns a promise,
 * independent of this replicated datatype being a strongly consistent replica or a strong eventually consistent replica.
 * Therefore, the below handler wraps return values in promises that are immediately resolved.
 */
function delegateToBaseObject(owningService, name) {
    return {
        // target is the replica (i.e. the CvRDT)
        get(target, prop, receiver) {
            var baseObject = (<Replica>target).getBaseObject();
            /*
            if (prop === '__metaObject__') {
                return owningService._REPLICAS_[name];
            }*/
            if (prop === 'onUpdate' || prop === 'onRemoteUpdate') {
                return function(...args) {
                    return target[prop](...args);
                }
            }
            else if (typeof baseObject[prop] !== 'function' && typeof prop !== 'symbol') {
                /*
                 * The user tries to access the replica's internal state directly.
                 * Deny this access since it would bypass the functioning of the CRDT and forms a threat for consistency.
                 */
                throw new ReferenceError("Cannot access a replica's internal state.");
            }
            else {
                return function (...args) {
                    if (typeof prop === 'symbol') {
                        // User accessed this proxy without calling a function (e.g. `myService.myEventualField`)
                        //return util.wrapInPromise(deserialize(serialize(baseObject))); // return a copy of the base object
                        return deserialize(serialize(baseObject)); // return a copy of the base object
                    }
                    else {
                        // User accessed this proxy as follows: `someService.aField.anOperation(someArgs)`
                        var op = baseObject[prop];
                        if (!op) { throw new Error(`${name}.${prop} is undefined.`); }
                        var baseCopy = deserialize(serialize(baseObject)); // deep copy
                        
                        // Make a copy of each argument and make it immutable
                        var immutableArgs = args.map(v => util.deepFreeze(deserialize(serialize(v))));
                        var res = op.apply(baseObject, immutableArgs);

                        try {
                            checkReturnValue(res, name, prop);
                        } catch(e) {
                            /*
                             * The method returned a reference to itself or one of its nested replicas.
                             * This is prohibited as it forms a threat for the consistency of the replica.
                             * The user should never be able to acquire a direct reference to a (nested) replica !
                             */
                            (<Replica>target).setBaseObject(baseCopy); // undo the operation
                            throw e;
                        }
                        
                        if (!baseObject.equals(baseCopy)) {
                            /* 
                             * The operation mutated the base object.
                             * Call the 'onUpdate' listener.
                             * Send update to everyone, but only send the new state.
                             * Therefore we make a minimal service that only contains the mutated replica.
                             */
                            (<Replica>target).notifyObserver();
                            var minimalService = new Service(owningService._TYPETAG_, owningService._ID_);
                            minimalService[name] = minimalService.makeCvRDT(name, baseObject);
                            
                            // Broadcast this update to everyone
                            broadcastUpdate(null, minimalService);
                        }
                        
                        //return util.wrapInPromise(res);
                        return res;
                    }
                };
            }
        }
    };
}

/*
 * This is the handler that is being used for the proxy of CmRDTs (i.e. operation-based CRDTs).
 * This handler traps all accesses and method invocations and delegates this to the underlying base object.
 * When it observes a mutation of the CmRDT (i.e. when to operation history changed), it broadcasts the operation to everyone.
 * It also provides access to the underlying `SECRO` object through the special `__metaObject__` accessor.
 *
 * In order to unify the APIs of both consistency models (strong consistency and strong eventual consistency),
 * accessing fields or invoking methods on a replicated datatype returns a promise,
 * independent of this replicated datatype being a strongly consistent replica or a strong eventually consistent replica.
 * Therefore, the below handler wraps return values in promises that are immediately resolved.
 */
function CmRDTHandler(owningService, name) {
    return {
        // `target` is the replica (i.e. the JSON CRDT)
        get(target, prop, receiver) {
            const baseObject = (<Replica>target).getBaseObject();
            if (prop === '__metaObject__') {
                return target;
            }
            if (prop === '__addProperty__' || prop === '__removeProperty__') {
                return function (...args) {
                    var res;
                    var immutableArgs = args.map(v => util.deepFreeze(deserialize(serialize(v))));
                    if (prop === '__addProperty__')
                        res = target.add(...immutableArgs);
                    else
                        res = target.remove(...immutableArgs);
                    
                    // Broadcast operation to all replicas
                    var {operation, resultObject} = res;
                    broadcastOperation(owningService._ID_, name, operation);
                    return resultObject;
                }
            }
            else if (prop === 'onUpdate' || prop === 'onRemoteUpdate') {
                return function(...args) {
                    return target[prop](...args);
                }
            }
            else if (prop === 'commit') {
                return function() {
                    /*
                     * Replay the current operation history,
                     * then pass the resulting object as an argument to commit.
                     * Commit will then commit this resulting object as being the new state and clear the operation history.
                     */
                    const history = (<SECRO>target).getRawHistory(),
                          {isValid, resultObject, _result} = (<SECRO>target).replayOperations(history);

                    if (!isValid) throw new Error("The current operation history is not valid.");

                    const {operation, result} = (<SECRO>target).execute('commit', [resultObject]);
                    // Broadcast the commit to all replicas
                    broadcastOperation(owningService._ID_, name, operation);

                    return result;
                }
            }
            else if (typeof baseObject[prop] === 'function') {
                return function (...args) {
                    var immutableArgs = args.map(v => util.deepFreeze(deserialize(serialize(v))));
                    if (target.isAccessor(prop)) {
                        /*
                         * Accessors are side-effect free and do not mutate the base object or any nested object/replica.
                         * Hence, we directly execute this method on the base object
                         * (after replaying the history on the initial base object),
                         * such that this call does not get added to the operation history.
                         */

                        // Replay the operation history
                        var curr = (<SECRO>target).replayOperations(target.getRawHistory()).resultObject; // Base object after replaying the history
                        var res  = curr[prop](...immutableArgs);

                        checkReturnValue(res, name, prop); // Safety check

                        //return util.wrapInPromise(res);
                        return res;
                    }
                    else {
                        var {operation, result} = target.execute(prop, immutableArgs);

                        try {
                            checkReturnValue(result, name, prop);
                        } catch (e) {
                            target.removeOperation(operation); // Illegal return value, undo the operation
                            throw e;
                        }

                        // Broadcast the operation to all replicas
                        broadcastOperation(owningService._ID_, name, operation);
                        //return util.wrapInPromise(result);

                        return result;
                    }
                }
            }
            else {
                /*
                 * The user tries to access the replica's internal state directly.
                 * Deny this access since it would bypass the functioning of the CRDT and forms a threat for consistency.
                 */
                throw new ReferenceError("Cannot access a replica's internal state.");
            }
        }
    };
}

/*
 * Checks the validity of a method's return value.
 * A replica method should never return a reference to a (nested) replica !
 */
function checkReturnValue(val, name, prop) {
    if (util.isReplica(val)) {
        /*
         * The method returns a reference to itself or one of its nested replicas.
         * This is prohibited as it forms a threat for the consistency of the replica.
         * The user should never be able to acquire a direct reference to a (nested) replica !
         */
        throw new ReferenceError(`For safety reasons ${name}.${prop} may not return a reference to a replica!`);
    }
}

export { setDependencies, Service, getService, addService, setBroadcast };