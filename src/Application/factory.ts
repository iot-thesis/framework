/*
 * Creates a generic factory for a given type.
 */

export default class Factory {
    private readonly Classes = {};
    private requiredMethods       : string[];
    private requiredStaticMethods : string[];
    
    constructor(checkMethodName : string, requiredMethods : string[], requiredStaticMethods : string[]) {
        this.requiredMethods       = requiredMethods;
        this.requiredStaticMethods = requiredStaticMethods;
        this[checkMethodName] = function(className : string): boolean {
            return this.Classes[className] ? true : false;
        }
    }
    
    factory(type : string): any {
        var clazz = this.Classes[type];
        
        if (!clazz) {
            throw new TypeError(`Factory has no class named ${type}.\nMake sure to register that class at the factory.`);
        }
        
        return clazz;
    }
    
    register(clazz : any): void {
        if (!clazz) { throw new TypeError(`Factory expects a class, but got '${clazz}'`); }
        if (clazz.name === "") { throw new TypeError(`Cannot register anonymous classes.`); }
        this.requiredMethods.forEach(method => {
            if (typeof clazz.prototype[method] !== 'function') {
                throw new TypeError(`Could not register ${clazz.name}: does not implement the '${method}' method from the required interface.`);
            }
        });
        
        this.requiredStaticMethods.forEach(method => {
            // Check that the class implements this static method
            if (typeof clazz[method] !== 'function') {
                throw new TypeError(`Could not register ${clazz.name}: does not implement the static '${method}' method from the required interface.`);
            }
        });
        
        // All tests passed, hence, the class implements all required methods
        this.Classes[clazz.name] = clazz;
    }
}