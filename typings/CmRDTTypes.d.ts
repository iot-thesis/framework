///<reference path='../node_modules/immutable/dist/immutable.d.ts'/>

type UUID             = string;
type Clock            = number;
type VC               = Map<UUID, Clock>;
type OperationHistory = Immutable.List<Immutable.List<Operation>>

declare class SECRO extends Replica {
    protected _id : UUID;
    private   _replicaCtr : number;
    
    private _initialBaseObject : Object;
    private _clock             : VectorClock;
    private _operationHistory  : OperationHistory;
    private _preconditions     : Conditions;
    private _postconditions    : Conditions;
    
    constructor(object : Object, preconditions : Conditions, postconditions : Conditions, id? : UUID, replicaID? : number);
    
    execute(op : PropertyKey, args : any[]): {operation: Operation, result: any};
    receive(operation : Operation, noUpdate? : boolean): {resultObject: Object, result: any};
    private insert(operation : Operation): {resultObject: Object, result: any};
    private getHistory(history? : OperationHistory): Immutable.List<Operation>;
    public getRawHistory(): OperationHistory;
    public replayOperations(history : OperationHistory, addedOperation? : Operation);
    add(property : PropertyKey, value): Object;
    delete(property : PropertyKey): Object;
    getObject();
    replicate(simulating? : boolean);
}

declare class Replica {
    public    name : string;
    protected _baseObject;
    private   _observer;
    private   _remoteObserver;

    constructor(name, val);
    getBaseObject();
    setBaseObject(o);
    onUpdate(observer);
    onRemoteUpdate(observer);
    notifyObserver(newState?);
    notifyRemoteObserver(newState);
    static fromReplica(owningService, name, replica);
}

declare class VectorClock implements Exchangeable<VectorClock> {
    constructor();
    get(): VC;
    update(): VC;
    update(vector : VectorClock): VC;
    static getVectorEntry(id : UUID, vector : VC): Clock;
    static happenedBefore(vector : VectorClock, otherVector : VectorClock): boolean;
    static isConcurrent(vector: VectorClock, otherVector: VectorClock): boolean;
    copy() : VectorClock;
    tojson(): any;
    static fromjson(vector : any): VectorClock;
}

declare class Operation {
    readonly clock    : VectorClock;
    public id         : string;
    readonly name     : PropertyKey;
    readonly args     : PrimOrExchangeable<any>[];
    readonly objectID : UUID;
    readonly version  : number;
    
    constructor(name : PropertyKey, args : PrimOrExchangeable<any>[], c : VectorClock, objectID : UUID);
    execute(baseObject : Object);
    equals(op : Operation): boolean;
    copy();
    toJSON(): any;
    static fromJSON(json : any): Operation;
}

declare class Conditions {
    //public has;
    //public get;
    protected _conditions : Map<PropertyKey, (state : Object, ...args: any[]) => boolean>;
    
    constructor();
    
    has(e);
    get(e);
    add(operation : PropertyKey, validator : (state : Object, ...args: any[]) => boolean): void;
    toJSON(): any;
    static fromJSON(json : any[]): Conditions;
}

declare class LWWSet<T> {
    private _addSets   : Map<T, Set<VectorClock>>;
    private _removeSets: Map<T, Set<VectorClock>>;
    constructor();
    has(elem : T, timestamp : VectorClock): boolean;
    add(elem : T, timestamp : VectorClock): void;
    remove(elem : T, timestamp : VectorClock): void;
    private getSets(elem : T);
    private addToSet(timestamp : VectorClock, elem : T, sets : Map<T, Set<VectorClock>>): void;
}