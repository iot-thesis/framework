type Primitive = boolean | number | string | symbol | null | undefined;

// Needed for merging replicas
interface Mergeable<T> {
    merge(replica : T): void;
}

// Needed for serialising and deserialising replicas
interface Exchangeable<T> {
    tojson(): any;
}

interface Eq<T> {
    equals(replica : T): boolean; // Needed for mutation testing (i.e. testing of a certain operation mutated a replica)
}

interface Mergeable<T> {
    merge(replica : T): void;
}

interface EqExchangeable<T> extends Eq<T>, Exchangeable<T> {}
interface CvRDT_I<T> extends Eq<T>, Exchangeable<T>, Mergeable<T> {}
interface CmRDT_I<T> extends Exchangeable<T> {}

type PrimOrExchangeable<T> = Primitive | Exchangeable<T>;

interface ObjectOfExchangeables<T> {
    [key: string]: Exchangeable<T>;
}