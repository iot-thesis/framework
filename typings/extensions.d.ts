interface MapConstructor {
    fromjson(json : any): any;
}

interface Array<T> {
    equals(replica : Array<T>): boolean;
}

interface Map<K, V> {
    getOrElse(key: K, value: V): V;
    tojson(): any;
    equals(replica : Map<K,V>): boolean;
}

interface SetConstructor {
    fromjson(json : any): any;
}

interface Set<T> {
    tojson(): any;
    equals(replica : Set<T>): boolean; 
}