// Type definitions for Consistency Script

//declare module "cscript" {
export class Service {
    _ID_ : string;
    _TYPETAG_ : string;
    _REPLICAS_: any;

    constructor(typeTag? : string, id? : string);

    makeEC<V>(name : string, baseObj : CvRDT_I<V> | CmRDT_I<V>);
    makeSC(name : string, baseObj : any);
    makeSCProxy(name : string, owner : string);

    publish(typeTag : string);
    static subscribe(typeTag, callback);
    toString();
    static fromString(str);
}

export interface Factory {
    registerExchangeableClass(clazz : any): void;
    registerCvRDTClass(clazz : any): void;
    registerCmRDTClass(clazz : any): void;
}

/*
export namespace Factory {
    export function registerExchangeableClass(clazz : any): void;
    export function registerCvRDTClass(clazz : any): void;
    export function registerCmRDTClass(clazz : any): void;
    //var registerExchangeableClass: (clazz : any) => void;
    //var registerCvRDTClass: (clazz : any) => void;
    //var registerCmRDTClass: (clazz : any) => void;
} */

export interface Datastructures {
    CRDTs: {
        GMap: GMap;
        ORSet: ORSet;
        LWWSet: LWWSet;
    };
}

/*
export namespace Datastructures {
    export namespace CRDTs {
        export var GMap: GMap;
        export var ORSet: ORSet;
        export var LWWSet: LWWSet;
    }

    export namespace Extra {
        export var Conditions: Conditions;
        export var VectorClock: VectorClock;
    }
}*/
//}

declare class GMap<K extends Primitive, V extends Exchangeable<V>|Primitive> extends Map<K,V> implements CvRDT_I<GMap<K,V>> {
    constructor(it? : Iterable<any>);
    
    set(key : K, val : V);
    delete(key : K): boolean;
    merge(replica : GMap<K, V>): void;
    equals(replica : GMap<K, V>): boolean;
    tojson(): any;
    //fromJSON(json : any[]) : GMap<K, V>;
}

declare class ORSet<T extends Primitive> implements CvRDT_I<ORSet<T>> {
    private addTags : Map<T, Set<string>>;
    private removeTags : Map<T, Set<string>>;
    private ops : any;
    
    constructor(addTags? : Map<T, string>, removeTags? : Map<T, string>, ops? : any);
    
    add(elem : T): boolean;
    delete(elem : T): boolean;
	has(elem : T): boolean;
	merge(replica : ORSet<T>): void;
    equals(replica : ORSet<T>): boolean;
    tojson(): any;
    // static fromjson<T extends Primitive> (json : any): ORSet<T>;
}

/*
declare class EArray<T extends Primitive|Exchangeable<T>> extends Array<T> implements Exchangeable<EArray<T>> {
    constructor(...args);
    
    static from<T extends Primitive|Exchangeable<T>>(it): EArray<T>;
    equals(replica : EArray<T>): boolean;
    toJSON(): any;
    fromJSON(json : any): EArray<T>;
}

declare class EObject<T extends Primitive|Exchangeable<T>> extends Object implements Exchangeable<EObject<T>> {
    constructor(val? : T);
    
    static create<V extends Primitive|Exchangeable<V>>(propertiesObject): EObject<V>;
    addProperty(prop : string, val : T): EObject<T>;
    equals(replica : EObject<T>): boolean;
    toJSON(): any;
    fromJSON(json : any): EObject<T>;
} */