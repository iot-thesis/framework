# arguments are: <current_directory> <main file>, <out dir>

echo "Preparing...";

# Navigate to directory where the script has been called from
cd "$1"

# Create the out dir if it does not exist yet
mkdir -p "$3"

basePath=$(cd "$(npm root)/cscript"; pwd); #$(cd "$(npm bin)"; cd ../../; pwd);
filePath=$(cd "$(dirname "$2")"; pwd)/$(basename "$2");
outPath=$(cd "$(dirname "$3")"; pwd)/$(basename "$3");

# Copy all files into the out directory
cdir=$(dirname "$filePath"); # directory of the main file

for f in "$cdir/"* 
do
	if [ "$f" = "$outPath" ]
	then
		continue # skip <out dir>
	else
		cp -r "$f" "$outPath"
	fi
done

cd "$outPath"

mainFile=$(basename "$filePath");
mainFileName="${mainFile%.*}.js"; #ts

echo "Transpiling...";

# Process all consistency script files
for file in *.cs; do
	fileName=$(basename "$filePath");

	# Inject code to require the DSL only once (in the main file)
	#if [ $file = $fileName ]
	#then
		{ echo "require('$basePath/compiled/src/DSL');";
  	  	  cat "$file"; } > "$file".new
  	  	mv "$file"{.new,}
	#fi

	# Inject the necessary imports and requires
	{ echo "import { service, publish, subscribe, deftype, class } from '$basePath/transpiler.sjs';";
  	  cat "$file"; } > "$file".new
  	mv "$file"{.new,}

    # Since SweetJS cannot handle decorators
    # manually transform the `@accessor` decorator
    # to something SweetJS can handle, then compile using SweetJS
    # and finally convert back to `@accessor` in the compiled file.
    find "$file" |xargs perl -pi -e 's/\@accessor/\_\_CS\_\_accessor\_\_\(\) \{\}/g'

	# Compile consistency script file to typescript file
	"$(npm bin)"/sjs -o "${file%.cs}.ts" "$file"

    # Revert to `@accessor` in the compiled file
    find "${file%.cs}.ts" |xargs perl -pi -e 's/\_\_CS\_\_accessor\_\_\(\) \{\}/\@accessor/g'

    # Use the typescript compiler to compile the typescript files (i.e. JS + decorators) to regular javascript
    # Do not print the output because the user wrote regular JS code, so it would throw many errors when compiled with typescript
    "$(npm bin)"/tsc --allowJs --experimentalDecorators --target "ES6" "${file%.cs}.ts" > /dev/null 2>&1

    # Remove the intermediary typescript file, as well as the copied original cscript file
    rm "${file%.cs}.ts"
	rm "$file"
done

echo "Transpilation completed"

# Execute the compiled version of the provided main file
#echo "Launching $mainFile";
#node "$mainFileName"
#"$(npm bin)"/ts-node "$mainFileName"