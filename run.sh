basePath="$(cd "$(dirname "$1")"; pwd)/$(basename "$1")";
"$(npm bin)"/ts-node -O '{"noImplicitUseStrict": true, "target": "es6", "allowJs": true, "experimentalDecorators": true}' "$basePath"