import * as assert from 'assert';
import * as sim from './Simulator';
import {Character, TextEditor} from '../Examples/js/TextEditor/logic';
const fw = sim.fw;

fw.Factory.registerCmRDTClass(TextEditor);

class Doc extends fw.Service {
    public doc;
    constructor(initialContent?: string) {
        super();
        this.doc = super.makeReplica('doc', new TextEditor(initialContent));
    }
}

describe('Testing commits on CmRDTs.', () => {
    const peer1 = new sim.Peer(),
          peer2 = new sim.Peer(),
          peer3 = new sim.Peer(),
          typetag = 'TextDocument';

    // Peer 1 creates a service and publishes it
    peer1.addService('docService', new Doc());
    peer1.publish('docService', typetag);

    // Peer 2 & 3 subscribe to a service
    peer2.subscribe(typetag, service => {
        peer2.docService = service;
    });

    peer3.subscribe(typetag, service => {
        peer3.docService = service;
    });

    peer1.goOnline();
    peer2.goOnline();
    peer3.goOnline();

    // Set the IDs of both CmRDTs to different values (because by default the framework takes the ip address)
    // TODO: do this automatically in `addService` and upon receiving a service (`recvService`)
    peer1.docService.doc.__metaObject__._clock._ownID = peer1.id;
    peer2.docService.doc.__metaObject__._clock._ownID = peer2.id;
    peer3.docService.doc.__metaObject__._clock._ownID = peer3.id;

    const charA = new Character("a"),
          charB = new Character("b"),
          charC = new Character("c");

    it('Sequential commits.', () => {
        // Do some tests
        peer1.docService.doc.insertAfter(null, charA);
        peer1.docService.doc.commit();

        peer2.docService.doc.insertAfter(charA.id, charB);
        //peer2.docService.doc.commit();

        assert.equal(peer1.docService.doc.getContent(), peer2.docService.doc.getContent());
        assert.equal(peer1.docService.doc.getContent(), peer3.docService.doc.getContent())
        assert.equal(peer1.docService.doc.getContent(), "ab");
    });

    it('Concurrent commits.', () => {
        // Tests the case where two commits happen concurrently
        // Both peers will agree on the commit with the smallest ID.
        // Note that all operations that happened concurrently with the commit will be dropped.
        peer2.goOffline();

        // Concurrently peer 1 commits his current state "ab",
        // and peer 2 inserts a new character yielding the state "abc",
        // after which peer 2 also commits.
        // Both commits will be concurrent and since peer 1's commit has the smallest ID his commit will be agreed on.
        // This implies that the insertion of "c" is lost since it happened concurrently with peer 1's commit.
        peer1.docService.doc.commit(); // commit "ab"
        peer2.docService.doc.insertAfter(charB.id, charC); // local state is "abc"
        peer2.docService.doc.commit(); // commit "abc"

        peer2.goOnline(); // exchange the concurrent operations

        assert.equal(peer1.docService.doc.getContent(), peer2.docService.doc.getContent());
        assert.equal(peer1.docService.doc.getContent(), peer3.docService.doc.getContent());
        assert.equal(peer1.docService.doc.getContent(), "ab");
    });

    it('Operation concurrent with commit.', () => {
        peer1.goOnline();
        peer2.goOnline();
        peer3.goOffline();

        const charD = new Character("d"),
              charE = new Character("e");
        peer1.docService.doc.insertAfter(charB.id, charC);
        peer2.docService.doc.insertAfter(charC.id, charD);

        // Concurrently peer 3 clears the document
        peer3.docService.doc.clearDocument();

        peer3.goOnline(); // exchange edits

        // Resulting state should be: "cd" (i.e. peer 3's state "ab" is deleted and the concurrently added characters "cd" remain)
        assert.equal(peer1.docService.doc.getContent(), "cd");
        assert.equal(peer2.docService.doc.getContent(), "cd");
        assert.equal(peer3.docService.doc.getContent(), "cd");

        // Peer 3 loses his connection again
        peer3.goOffline();

        // Concurrently peer 3 commits the "cd" state, while peer 2 inserts "e"
        peer3.docService.doc.commit();
        peer2.docService.doc.insertAfter(charD.id, charE);

        peer3.goOnline();

        // Since the insertion of "e" is concurrent with the commit, the operation should be dropped
        assert.equal(peer1.docService.doc.getContent(), "cd");
        assert.equal(peer2.docService.doc.getContent(), "cd");
        assert.equal(peer3.docService.doc.getContent(), "cd");
    });
});