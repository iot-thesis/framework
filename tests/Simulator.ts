import * as fw from '../src/framework';
import {deserialize, serialize} from "../src/Application/serialization";
const Service = fw.Service;

function broadcastOperation(peer: Peer, serviceID : string, replicaName : PropertyKey, operation : Operation) {
    peer.broadcastOperation(serviceID, replicaName, operation);
}

type TypeTag = string;
type Identifier = string;
enum Connectivity { ONLINE, OFFLINE }

class Peer {
    static ctr = 1; // static peer counter
    static publications  = new Map(); // Map<TypeTag, Set<Service>>
    static subscriptions = new Map(); // Map<TypeTag, Set<Peer>>
    static peers = [];

    private services: Map<Identifier, Service>; // DCT of names to services
    private publications: Set<Service>;
    private subscriptions: Map<TypeTag, (s: Service) => any>;
    private connectivityStatus: Connectivity;
    private buffer: any[];

    constructor(readonly id: number = Peer.ctr++) {
        this.services = new Map();
        this.publications = new Set();
        this.subscriptions = new Map();
        this.connectivityStatus = Connectivity.OFFLINE;
        this.buffer = [];
        Peer.peers.push(this);
    }

    // Adds a service on the peer
    addService(name: Identifier, service: Service) {
        if (!(service instanceof Service)) throw new TypeError("Argument to `addService` is not a service.");
        this.services.set(name, service);
        return service;
    }

    // Publishes the service with the given name under the given typetag
    publish(name: Identifier, typetag: TypeTag) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            if (!this.services.has(name)) throw new Error(`Peer ${this.id} has no service named ${name}.`);

            const service = this.services.get(name);
            if (this.publications.has(service)) return; // already published

            if (Peer.publications.has(typetag)) {
                var setOfPublications: Set<Service> = Peer.publications.get(typetag);
                setOfPublications.add(service);
            }
            else {
                Peer.publications.set(typetag, new Set([service]));
            }

            // Send this service to all peers that subscribed to it
            if (Peer.subscriptions.has(typetag)) {
                const setOfPeers: Set<Peer> = Peer.subscriptions.get(typetag);
                setOfPeers.forEach(peer => peer.recvService(service, typetag));
            }
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.publish(name, typetag));
        }
    }

    subscribe(typetag: TypeTag, onPublication: (s: Service) => any) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            if (this.subscriptions.has(typetag)) return; // already subscribed

            if (Peer.subscriptions.has(typetag)) {
                const setOfPeers: Set<Peer> = Peer.subscriptions.get(typetag);
                setOfPeers.add(this);
            }
            else {
                Peer.subscriptions.set(typetag, new Set([this]));
            }

            // Check services that were already published
            if (Peer.publications.has(typetag)) {
                this.subscriptions.set(typetag, onPublication);
                const setOfServices: Set<Service> = Peer.publications.get(typetag);
                setOfServices.forEach(service => {
                    const serviceCopy: Service  = service; //Service.fromString(service.toString());
                    this.recvService(serviceCopy, typetag);
                    //onPublication(serviceCopy);
                });
            }
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.subscribe(typetag, onPublication));
        }
    }

    // Receives a service
    recvService(service: Service, typetag: TypeTag) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            if (this.subscriptions.has(typetag)) {
                const serviceCopy = Service.fromString(service.toString());
                this.services.set(service._ID_, serviceCopy); // also store received services (under some unique identifier)
                const onPublication: (s: Service) => any = this.subscriptions.get(typetag);
                onPublication(serviceCopy);
            }
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.recvService(service, typetag));
        }
    }

    // Delivers the operation to the correct service
    receiveOperation(msg) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            const {serviceID, name, op} = JSON.parse(msg),
                  operation = deserialize(op);
            this.services.forEach(service => {
                if (service._ID_ === serviceID) {
                    service._REPLICAS_[name].receive(operation);
                }
            });
        }
        else {
            // Store this call in the buffer
            this.buffer.push(() => this.receiveOperation(msg));
        }
    }

    broadcastOperation(serviceID : string, replicaName : PropertyKey, operation : Operation) {
        if (this.connectivityStatus === Connectivity.ONLINE) {
            var infoMessage = JSON.stringify({ serviceID: serviceID, name: replicaName, op: serialize(operation) });
            Peer.peers.forEach(peer => {
                if (peer.id !== this.id)
                    peer.receiveOperation(infoMessage)
            });
        }
        else {
            this.buffer.push(() => this.broadcastOperation(serviceID, replicaName, operation));
        }
    }

    // Makes the peer go online (will execute all lambda's stored in `buffer`)
    goOnline() {
        this.connectivityStatus = Connectivity.ONLINE;
        this.buffer.forEach(action => action());
        this.buffer = [];
    }

    goOffline() {
        this.connectivityStatus = Connectivity.OFFLINE;
    }
}

// Proxies
const peerHandler = {
    get(target, prop, receiver) {
        fw.Config.setBroadcastMechanism(target.broadcastOperation.bind(target));
        if (target.hasOwnProperty(prop) || typeof target[prop] === 'function')
            return target[prop];
        else
            return target.services.get(prop);
    }
};

const PeerProxy = new Proxy(Peer, {
    construct(target, args) {
        const peer = new target(...args);
        // Return a proxy on the peer
        return new Proxy(peer, peerHandler);
    }
});

export { PeerProxy as Peer, fw };